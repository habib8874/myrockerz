﻿using Nop.Web.Framework.Models;
using System.Collections.Generic;

namespace Nop.Web.Models.Checkout
{
    public partial class CheckoutPaymentInfoModel : BaseNopModel
    {
        public CheckoutPaymentInfoModel()
        {
            Warnings = new List<string>();
        }

        public string PaymentViewComponentName { get; set; }

        /// <summary>
        /// Used on one-page checkout page
        /// </summary>
        public bool DisplayOrderTotals { get; set; }

        #region Custom

        public IList<string> Warnings { get; set; }

        #endregion
    }
}