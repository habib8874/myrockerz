﻿using System.Collections.Generic;
using System;
using Nop.Web.Models.Media;
using Nop.Web.Framework.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Web.Models.ModelPopupWebs
{
    public class Sinek_CutPointWebModel : BaseNopEntityModel
    {
        //class Sinek_CutPointModel
        //{
        //    //all the stuff required in TopMenu.aspx
        //    Sinek_CutPointModel sinek_CutPointModelMorge;

        //    Nop.Web.Models.Catalog.ProductModel.ProductVariantModel.AddToCartModel resultvm;
        //}

        public Sinek_CutPointWebModel()
        {
            if (PageSize < 1)
            {
                PageSize = 5;
            }
            Locales = new List<Sinek_CutPointLocalizedModel>();
            AvailableManufacturerId = new List<SelectListItem>();
            AvailableModelId = new List<SelectListItem>();
            AvailableCutPoint = new List<SelectListItem>();
            AvailableSize = new List<SelectListItem>();
            ModalPictureModel = new PictureModel();
        }

        public PictureModel ModalPictureModel { get; set; }
        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.Id")]
        public int Id { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.ManufacturerId")]
        public int ManufacturerId { get; set; }
        public IList<SelectListItem> AvailableManufacturerId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.ModelId")]
        public int ModelId { get; set; }
        public IList<SelectListItem> AvailableModelId { get; set; }

        public IList<SelectListItem> AvailableCutPoint { get; set; }

        public IList<SelectListItem> AvailableSize { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.Size")]
        public string Size { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.CutPoint")]
        public string CutPoint { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.AdditionalInfo")]
        public string AdditionalInfo { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.PageSize")]
        public int PageSize { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.PageSizeOptions")]
        public string PageSizeOptions { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.Published")]
        public bool Published { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.Deleted")]
        public bool Deleted { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.CreatedOnUtc")]
        public DateTime CreatedOnUtc { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.UpdatedOnUtc")]
        public DateTime UpdatedOnUtc { get; set; }

        public IList<Sinek_CutPointLocalizedModel> Locales { get; set; }

        public bool HideNameAndDescriptionProperties { get; set; }
        public bool HidePublishedProperty { get; set; }
        public bool HideDisplayOrderProperty { get; set; }

    }

    public class Sinek_CutPointLocalizedModel : ILocalizedLocaleModel
    {
        public int LanguageId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.Id")]
        public int Id { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.ManufacturerId")]
        public int ManufacturerId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.ModelId")]
        public int ModelId { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.Size")]
        public string Size { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.CutPoint")]
        public string CutPoint { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.AdditionalInfo")]
        public string AdditionalInfo { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.PageSize")]
        public int PageSize { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.PageSizeOptions")]
        public string PageSizeOptions { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.Published")]
        public bool Published { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.Deleted")]
        public bool Deleted { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.CreatedOnUtc")]
        public DateTime CreatedOnUtc { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_CutPoints.Fields.UpdatedOnUtc")]
        public DateTime UpdatedOnUtc { get; set; }
    }
}