﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Models.Catalog
{
	public class LocationForGoogleMap : BaseNopEntityModel
    {
        public LocationForGoogleMap()
        {
        }
        public string LocationAddress { get; set; }
    }
}