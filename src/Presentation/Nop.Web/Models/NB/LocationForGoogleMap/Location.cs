﻿namespace Nop.Web.Models.Catalog
{
	public class Location
    {
        public Location(int id, string name1, string locationAddress)
        {
            this.ID = id;
            this.LocationAddress = locationAddress;
            this.Name1 = name1;
        }

        public int ID { get; set; }
        public string Name1 { get; set; }
        public string LocationAddress { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string GetMap { get; set; }
    }
}