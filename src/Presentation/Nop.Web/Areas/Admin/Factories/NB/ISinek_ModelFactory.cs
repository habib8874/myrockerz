﻿using Nop.Core.Domain.NB;
using Nop.Web.Areas.Admin.Models.NB;

namespace Nop.Web.Areas.Admin.Factories.NB
{
	/// <summary>
	/// Represents the sinekmodel model factory
	/// </summary>
	public partial interface ISinek_ModelFactory
	{
		/// <summary>
		/// Prepare sinekmodel search model
		/// </summary>
		/// <param name="searchModel">Sinek_Model_View search model</param>
		/// <returns>Sinek_Model_View search model</returns>
		Sinek_Model_ViewSearchModel PrepareSinekModelViewSearchModel(Sinek_Model_ViewSearchModel searchModel);

		/// <summary>
		/// Prepare paged sinekmodelview list model
		/// </summary>
		/// <param name="searchModel">SinekModelView search model</param>
		/// <returns>SinekModelView list model</returns>
		Sinek_Model_ViewListModel PrepareSinekModelViewListModel(Sinek_Model_ViewSearchModel searchModel);

		/// <summary>
		/// Prepare sinekModel model
		/// </summary>
		/// <param name="model">sinekModel model</param>
		/// <param name="category">sinekModel</param>
		/// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
		/// <returns>Sinek_Model model</returns>
		Sinek_ModelModel PrepareSinekModelModel(Sinek_ModelModel model, Sinek_Model sinekModel, bool excludeProperties = false);
	}
}
