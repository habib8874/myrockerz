﻿using Nop.Core.Domain.NB;
using Nop.Web.Areas.Admin.Models.NB;

namespace Nop.Web.Areas.Admin.Factories.NB
{
	/// <summary>
	/// Represents the sinekLocation model factory
	/// </summary>
	public partial interface ISinek_LocationModelFactory
	{
		/// <summary>
		/// Prepare sinekLocation search model
		/// </summary>
		/// <param name="searchModel">Sinek_Location search model</param>
		/// <returns>Sinek_Location search model</returns>
		Sinek_LocationSearchModel PrepareSinekLocationSearchModel(Sinek_LocationSearchModel searchModel);

		/// <summary>
		/// Prepare paged sinekLocation list model
		/// </summary>
		/// <param name="searchModel">sinekLocation search model</param>
		/// <returns>sinekLocation list model</returns>
		Sinek_LocationListModel PrepareSinekLocationListModel(Sinek_LocationSearchModel searchModel);

		/// <summary>
		/// Prepare sinekLocation model
		/// </summary>
		/// <param name="model">sinekLocation model</param>
		/// <param name="sinekLocation">sinekLocation entity</param>
		/// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
		/// <returns>Sinek_Location model</returns>
		Sinek_LocationModel PrepareSinekLocationModel(Sinek_LocationModel model, Sinek_Location sinekLocation, bool excludeProperties = false);
	}
}
