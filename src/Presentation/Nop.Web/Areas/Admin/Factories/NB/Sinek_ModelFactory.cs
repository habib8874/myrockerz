﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.NB;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.NB;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.NB;
using Nop.Web.Framework.Factories;
using Nop.Web.Framework.Models.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nop.Web.Areas.Admin.Factories.NB
{
	/// <summary>
	/// Represents the sinekmodel model factory implementation
	/// </summary>
	public partial class Sinek_ModelFactory : ISinek_ModelFactory
	{
		#region Fields

		private readonly ISinek_ModelService _sinek_ModelService;
		private readonly ILocalizationService _localizationService;
		private readonly CatalogSettings _catalogSettings;
		private readonly ILocalizedModelFactory _localizedModelFactory;
		private readonly IManufacturerService _manufacturerService;
		private readonly IBaseAdminModelFactory _baseAdminModelFactory;

		#endregion

		#region Ctor

		public Sinek_ModelFactory(ISinek_ModelService sinek_ModelService,
			ILocalizationService localizationService,
			CatalogSettings catalogSettings,
			ILocalizedModelFactory localizedModelFactory,
			IManufacturerService manufacturerService,
			IBaseAdminModelFactory baseAdminModelFactory)
		{
			_sinek_ModelService = sinek_ModelService;
			_localizationService = localizationService;
			_catalogSettings = catalogSettings;
			_localizedModelFactory = localizedModelFactory;
			_manufacturerService = manufacturerService;
			_baseAdminModelFactory = baseAdminModelFactory;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Prepare sinekmodel search model
		/// </summary>
		/// <param name="searchModel">Sinek_Model_View search model</param>
		/// <returns>Sinek_Model_View search model</returns>
		public virtual Sinek_Model_ViewSearchModel PrepareSinekModelViewSearchModel(Sinek_Model_ViewSearchModel searchModel)
		{
			if (searchModel == null)
				throw new ArgumentNullException(nameof(searchModel));

			//prepare available manufactures
			_baseAdminModelFactory.PrepareManufacturers(searchModel.AvailableManufacturers);

			//prepare page parameters
			searchModel.SetGridPageSize();

			return searchModel;
		}

		/// <summary>
		/// Prepare paged sinekmodel list model
		/// </summary>
		/// <param name="searchModel">SinekModel search model</param>
		/// <returns>SinekModel list model</returns>
		public virtual Sinek_Model_ViewListModel PrepareSinekModelViewListModel(Sinek_Model_ViewSearchModel searchModel)
		{
			if (searchModel == null)
				throw new ArgumentNullException(nameof(searchModel));

			//get records
			var sinekModelsViews = _sinek_ModelService.GetAllSinek_Models_View(searchModel.Name, searchModel.ManufacturerId, 
								   pageIndex: searchModel.Page - 1, pageSize: searchModel.PageSize, showHidden: true);

			//prepare grid model
			var model = new Sinek_Model_ViewListModel().PrepareToGrid(searchModel, sinekModelsViews, () =>
			{
				return sinekModelsViews.Select(sm =>
				{
					//fill in model values from the entity
					var sinekViewModel = sm.ToModel<Sinek_Model_ViewModel>();

					return sinekViewModel;
				});
			});

			return model;
		}

		/// <summary>
		/// Prepare sinekModel model
		/// </summary>
		/// <param name="model">sinekModel model</param>
		/// <param name="sinekModel">sinekModel</param>
		/// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
		/// <returns>Sinek_Model model</returns>
		public virtual Sinek_ModelModel PrepareSinekModelModel(Sinek_ModelModel model, Sinek_Model sinekModel, bool excludeProperties = false)
		{
			Action<Sinek_ModelLocalizedModel, int> localizedModelConfiguration = null;

			if (sinekModel != null)
			{
				//fill in model values from the entity
				if (model == null)
				{
					model = sinekModel.ToModel<Sinek_ModelModel>();
				}

				//define localized model configuration action
				localizedModelConfiguration = (locale, languageId) =>
				{
					locale.Name = _localizationService.GetLocalized(sinekModel, entity => entity.Name, languageId, false, false);
					locale.Description = _localizationService.GetLocalized(sinekModel, entity => entity.Description, languageId, false, false);
				};
			}

			//set default values for the new model
			if (sinekModel == null)
			{
				model.PageSize = 4;
				model.PageSizeOptions = _catalogSettings.DefaultSinek_ModelPageSizeOptions;
				model.Published = true;
			}

			//prepare localized models
			if (!excludeProperties)
				model.Locales = _localizedModelFactory.PrepareLocalizedModels(localizedModelConfiguration);

			//prepare available manufactures
			_baseAdminModelFactory.PrepareManufacturers(model.AvailableManufacturerId,
				defaultItemText: "Select Blade Make");

			return model;
		}

		#endregion
	}
}
