﻿using Nop.Core.Domain.NB;
using Nop.Web.Areas.Admin.Models.NB;

namespace Nop.Web.Areas.Admin.Factories.NB
{
	/// <summary>
	/// Represents the sinekCutpoint model factory
	/// </summary>
	public partial interface ISinek_CutPointModelFactory
	{
		/// <summary>
		/// Prepare sinekCutpoint search model
		/// </summary>
		/// <param name="searchModel">Sinek_CutPoint_View search model</param>
		/// <returns>Sinek_CutPoint_View search model</returns>
		Sinek_CutPoint_ViewSearchModel PrepareSinekCutpointViewSearchModel(Sinek_CutPoint_ViewSearchModel searchModel);

		/// <summary>
		/// Prepare paged sinekCutpointmodelview list model
		/// </summary>
		/// <param name="searchModel">sinekCutpointmodelView search model</param>
		/// <returns>sinekCutpointView list model</returns>
		Sinek_CutPoint_ViewListModel PrepareSinekCutpointViewListModel(Sinek_CutPoint_ViewSearchModel searchModel);

		/// <summary>
		/// Prepare sinekCutpoint model
		/// </summary>
		/// <param name="model">sinekCutpointmodel model</param>
		/// <param name="sinekCutpoint">sinekCutpointmodel</param>
		/// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
		/// <returns>Sinek_CutPoint model</returns>
		Sinek_CutPointModel PrepareSinekCutpointModel(Sinek_CutPointModel model, Sinek_CutPoint sinekCutpoint, bool excludeProperties = false);
	}
}
