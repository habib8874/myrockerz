﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.NB;
using Nop.Services.Catalog;
using Nop.Services.ExportImport;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.NB;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Factories.NB;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.NB;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.AspNetCore.Hosting;
using Nop.Core;
using Microsoft.AspNetCore.Http;

namespace Nop.Web.Areas.Admin.Controllers.NB
{
	public class Sinek_CutPointController : BaseAdminController
	{
		#region Fields

		private readonly ISinek_CutPointService _sinek_CutPointService;
		private readonly IManufacturerService _manufacturerService;
		private readonly ISinek_ModelService _sinek_ModelService;
		private readonly ILanguageService _languageService;
		private readonly ILocalizationService _localizationService;
		private readonly ILocalizedEntityService _localizedEntityService;
		private readonly IExportManager _exportManager;
		private readonly IPermissionService _permissionService;
		private readonly AdminAreaSettings _adminAreaSettings;
		private readonly CatalogSettings _catalogSettings;
		private readonly IImportManager _importManager;
		private readonly ISinek_CutPointModelFactory _sinek_CutPointModelFactory;
		private readonly INotificationService _notificationService;
		private readonly IHostingEnvironment _hostingEnvironment;
		private readonly IWorkContext _workContext;

		#endregion

		#region Ctor

		public Sinek_CutPointController(
			IManufacturerService manufacturerService,
			ISinek_ModelService sinek_ModelService,
			ILanguageService languageService,
			ILocalizationService localizationService,
			ILocalizedEntityService localizedEntityService,
			IExportManager exportManager,
			AdminAreaSettings adminAreaSettings,
			CatalogSettings catalogSettings,
			ISinek_CutPointService sinek_CutPointService,
			IPermissionService permissionService,
			IImportManager importManager,
			ISinek_CutPointModelFactory sinek_CutPointModelFactory,
			INotificationService notificationService,
			IHostingEnvironment hostingEnvironment,
			IWorkContext workContext)
		{
			_manufacturerService = manufacturerService;
			_sinek_ModelService = sinek_ModelService;
			_languageService = languageService;
			_localizationService = localizationService;
			_localizedEntityService = localizedEntityService;
			_exportManager = exportManager;
			_adminAreaSettings = adminAreaSettings;
			_catalogSettings = catalogSettings;
			_sinek_CutPointService = sinek_CutPointService;
			_permissionService = permissionService;
			_importManager = importManager;
			_sinek_CutPointModelFactory = sinek_CutPointModelFactory;
			_notificationService = notificationService;
			_hostingEnvironment = hostingEnvironment;
			_workContext = workContext;
		}

		#endregion

		#region Utilities

		protected virtual void UpdateLocales(Sinek_CutPoint sinek_CutPoint, Sinek_CutPointModel model)
		{
			foreach (var localized in model.Locales)
			{
				_localizedEntityService.SaveLocalizedValue(sinek_CutPoint,
															   x => x.ManufacturerId,
															   localized.ManufacturerId,
															   localized.LanguageId);

				_localizedEntityService.SaveLocalizedValue(sinek_CutPoint,
														   x => x.ModelId,
														   localized.ModelId,
														   localized.LanguageId);

				_localizedEntityService.SaveLocalizedValue(sinek_CutPoint,
														   x => x.Size,
														   localized.Size,
														   localized.LanguageId);

				_localizedEntityService.SaveLocalizedValue(sinek_CutPoint,
														   x => x.CutPoint,
														   localized.CutPoint,
														   localized.LanguageId);

				_localizedEntityService.SaveLocalizedValue(sinek_CutPoint,
														   x => x.AdditionalInfo,
														   localized.AdditionalInfo,
														   localized.LanguageId);

				_localizedEntityService.SaveLocalizedValue(sinek_CutPoint,
														   x => x.DisplayOrder,
														   localized.DisplayOrder,
														   localized.LanguageId);
			}
		}

		#endregion

		#region List

		public IActionResult Index()
		{
			return RedirectToAction("List");
		}

		public IActionResult List()
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekCutPoint))
				return AccessDeniedView();

			//prepare model
			var model = _sinek_CutPointModelFactory.PrepareSinekCutpointViewSearchModel(new Sinek_CutPoint_ViewSearchModel());

			return View(model);
		}

		[HttpPost]
		public IActionResult List(Sinek_CutPoint_ViewSearchModel searchModel)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekCutPoint))
				return AccessDeniedView();

			//prepare model
			var model = _sinek_CutPointModelFactory.PrepareSinekCutpointViewListModel(searchModel);

			return Json(model);
		}

		#endregion

		#region Create / Edit / Delete

		public IActionResult Create()
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekCutPoint))
				return AccessDeniedView();

			//prepare model
			var model = _sinek_CutPointModelFactory.PrepareSinekCutpointModel(new Sinek_CutPointModel(), null);

			return View(model);

		}

		[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
		public IActionResult Create(Sinek_CutPointModel model, bool continueEditing)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekCutPoint))
				return AccessDeniedView();

			if (model.ManufacturerId == 0)
				ModelState.AddModelError("", "Please provide blade make.");

			if (ModelState.IsValid)
			{
				var sinek_CutPoint = model.ToEntity<Sinek_CutPoint>();
				sinek_CutPoint.CreatedOnUtc = DateTime.UtcNow;
				sinek_CutPoint.UpdatedOnUtc = DateTime.UtcNow;
				sinek_CutPoint.DisplayOrder = model.DisplayOrder;
				sinek_CutPoint.AdditionalInfo = model.AdditionalInfo;

				//insert
				_sinek_CutPointService.InsertSinek_CutPoint(sinek_CutPoint);

				//locales
				UpdateLocales(sinek_CutPoint, model);

				_notificationService.SuccessNotification(_localizationService.GetResource("Admin.Catalog.Sinek_CutPoints.Added"));
				return continueEditing ? RedirectToAction("Edit", new { id = sinek_CutPoint.Id }) : RedirectToAction("List");
			}

			//prepare model
			model = _sinek_CutPointModelFactory.PrepareSinekCutpointModel(model, null, true);

			//if we got this far, something failed, redisplay form
			return View(model);
		}

		public IActionResult Edit(int id)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekCutPoint))
				return AccessDeniedView();

			var sinek_CutPoint = _sinek_CutPointService.GetSinek_CutPointById(id);
			if (sinek_CutPoint == null || sinek_CutPoint.Deleted)
				//No manufacturer found with the specified id
				return RedirectToAction("List");

			//prepare model
			var model = _sinek_CutPointModelFactory.PrepareSinekCutpointModel(null, sinek_CutPoint);

			return View(model);
		}

		[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
		public IActionResult Edit(Sinek_CutPointModel model, bool continueEditing)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekCutPoint))
				return AccessDeniedView();

			var sinek_CutPoint = _sinek_CutPointService.GetSinek_CutPointById(model.Id);
			if (sinek_CutPoint == null || sinek_CutPoint.Deleted)
				//No manufacturer found with the specified id
				return RedirectToAction("List");

			if (model.ManufacturerId == 0)
				ModelState.AddModelError("", "Please provide blade make.");

			if (ModelState.IsValid)
			{
				sinek_CutPoint = model.ToEntity(sinek_CutPoint);
				sinek_CutPoint.UpdatedOnUtc = DateTime.UtcNow;
				sinek_CutPoint.AdditionalInfo = model.AdditionalInfo;
				sinek_CutPoint.ManufacturerId = model.ManufacturerId;
				sinek_CutPoint.DisplayOrder = model.DisplayOrder;

				_sinek_CutPointService.UpdateSinek_CutPoint(sinek_CutPoint);

				//locales
				UpdateLocales(sinek_CutPoint, model);

				_notificationService.SuccessNotification(_localizationService.GetResource("Admin.Catalog.Sinek_CutPoints.Updated"));

				return continueEditing ? RedirectToAction("Edit", sinek_CutPoint.Id) : RedirectToAction("List");
			}

			//prepare model
			model = _sinek_CutPointModelFactory.PrepareSinekCutpointModel(model, sinek_CutPoint, true);

			//if we got this far, something failed, redisplay form
			return View(model);
		}

		public IActionResult Delete(int id)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekCutPoint))
				return AccessDeniedView();

			var sinek_CutPoint = _sinek_CutPointService.GetSinek_CutPointById(id);
			if (sinek_CutPoint == null)
				//No manufacturer found with the specified id
				return RedirectToAction("List");

			_sinek_CutPointService.DeleteSinek_CutPoint(sinek_CutPoint);

			_notificationService.SuccessNotification(_localizationService.GetResource("Admin.Catalog.Sinek_CutPoints.Deleted"));

			return RedirectToAction("List");
		}

		public IActionResult RemoveCurrentSinekCutPointId(string id)
		{
			string result = "";

			try
			{
				if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekCutPoint))
					return AccessDeniedView();

				var sinek_CutPoint = _sinek_CutPointService.GetSinek_CutPointById(Convert.ToInt32(id));
				if (sinek_CutPoint == null)
					//No manufacturer found with the specified id
					return RedirectToAction("List");

				_sinek_CutPointService.DeleteSinek_CutPoint(sinek_CutPoint);

				//activity log
				//_customerActivityService.InsertActivity("DeleteManufacturer", _localizationService.GetResource("ActivityLog.DeleteManufacturer"), sinek_CutPoint.Name);

				_notificationService.SuccessNotification(_localizationService.GetResource("Admin.Catalog.Sinek_CutPoints.Deleted"));

				result = "Success";
			}
			catch
			{
				result = "Failure";
			}

			return Json(result);
		}

		#endregion

		#region Export / Import

		public IActionResult ExportXml()
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekCutPoint))
				return AccessDeniedView();

			try
			{
				DateTime dt = DateTime.Now;

				string sysDate = dt.Date.ToString();

				string year = dt.Date.ToString().Split('/')[2].ToString().ElementAt(3).ToString() + ".";
				string month = Convert.ToString(Convert.ToInt32(dt.Date.ToString().Split('/')[0])) + ".";
				string date = dt.Date.ToString().Split('/')[1].ToString();

				var sinek_CutPoints = _sinek_CutPointService.GetAllSinek_CutPoints(true);
				var xml = _exportManager.ExportSinek_CutPointsToXml(sinek_CutPoints, (year + month + date));


				//string fileName = "RockerzCutPoints-" + year + month + date + "-1.10.xml";
				string fileName = "RockerzCutPoints.xml";
				string filePath = System.IO.Path.Combine(_hostingEnvironment.WebRootPath, "files", fileName);

				//System.IO.File.WriteAllText(filePath, xml);
				XmlDocument doc = new XmlDocument();
				doc.LoadXml(xml);

				var writer = XmlTextWriter.Create(
					filePath,
					new XmlWriterSettings { Encoding = System.Text.ASCIIEncoding.ASCII });
				doc.Save(writer);
				_notificationService.SuccessNotification(_localizationService.GetResource("Admin.Catalog.CutPoints.Export"));
				return RedirectToAction("List");
			}
			catch (Exception exc)
			{
				_notificationService.ErrorNotification(exc);
				return RedirectToAction("List");
			}
		}

		//[HttpPost]
		//public IActionResult ImportXML(IFormFile importexcelfile)
		//{
		//	if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekCutPoint))
		//		return AccessDeniedView();

		//	try
		//	{
		//		var file = Request.Files["importxmlfile"];
		//		if (file.ContentType == "text/xml")
		//		{
		//			if (file != null && file.ContentLength > 0)
		//			{
		//				var fileBytes = new byte[file.ContentLength];
		//				file.InputStream.Read(fileBytes, 0, file.ContentLength);
		//				//do stuff with the bytes
		//				string fileName = string.Format("cutpoint_{0}_{1}.xml", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"), CommonHelper.GenerateRandomDigitCode(4));
		//				string filePath = System.IO.Path.Combine(_hostingEnvironment.WebRootPath, "content\\files\\ExportImport", fileName);

		//				System.IO.File.WriteAllBytes(filePath, fileBytes);
		//				_importManager.ImportCutPointsFromXlsx(filePath);
		//			}

		//		}
		//		else
		//		{
		//			_notificationService.ErrorNotification("Please upload XML file.");
		//			return RedirectToAction("List");
		//		}
		//		//else
		//		//{
		//		//    _notificationService.ErrorNotification(_localizationService.GetResource("Admin.Common.UploadFile"));
		//		//    return RedirectToAction("List");
		//		//}
		//		_notificationService.SuccessNotification(_localizationService.GetResource("Admin.Catalog.CutPoints.Imported"));
		//		return RedirectToAction("List");
		//	}
		//	catch (Exception exc)
		//	{
		//		_notificationService.ErrorNotification(exc);
		//		return RedirectToAction("List");
		//	}

		//}

		#endregion

		#region Other

		public IActionResult GetSinek_ModelByManufacturerId(string manufacturerId, bool addEmptyModelIfRequired)
		{
			//manufacturerId = "1";
			// This action method gets called via an ajax request
			if (String.IsNullOrEmpty(manufacturerId))
				throw new ArgumentNullException("manufacturerId");

			var manufacturer = _manufacturerService.GetManufacturerById(Convert.ToInt32(manufacturerId));

			var sinek_Models = manufacturer != null ? _sinek_ModelService.GetSinek_ModelByManufacturerId(manufacturer.Id).ToList() : new List<Sinek_Model>();
			var result = (from s in sinek_Models
						  select new { id = s.Id, name = _localizationService.GetLocalized(s, entity => entity.Name, _workContext.WorkingLanguage.Id, true, false) }).ToList();

			return Json(result);
		}

		#endregion
	}
}
