﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.NB;
using Nop.Services.Catalog;
using Nop.Services.ExportImport;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.NB;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Factories.NB;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.NB;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nop.Web.Areas.Admin.Controllers.NB
{
	public class Sinek_ModelController : BaseAdminController
	{
		#region Fields

		private readonly ISinek_ModelService _sinek_ModelService;
		private readonly ISinek_CutPointService _sinek_CutPointService;
		private readonly IManufacturerService _manufacturerService;
		private readonly ILocalizationService _localizationService;
		private readonly ILocalizedEntityService _localizedEntityService;
		private readonly IExportManager _exportManager;
		private readonly IPermissionService _permissionService;
		private readonly ISinek_ModelFactory _sinek_ModelFactory;
		private readonly INotificationService _notificationService;

		#endregion

		#region Ctor

		public Sinek_ModelController(
			ISinek_ModelService sinek_ModelService,
			ISinek_CutPointService sinek_CutPointService,
			IManufacturerService manufacturerService,
			ILanguageService languageService,
			ILocalizationService localizationService,
			ILocalizedEntityService localizedEntityService,
			IExportManager exportManager,
			IPermissionService permissionService,
			AdminAreaSettings adminAreaSettings,
			CatalogSettings catalogSettings,
			ISinek_ModelFactory sinek_ModelFactory,
			INotificationService notificationService)
		{
			_sinek_ModelService = sinek_ModelService;
			_sinek_CutPointService = sinek_CutPointService;
			_manufacturerService = manufacturerService;
			_localizationService = localizationService;
			_localizedEntityService = localizedEntityService;
			_exportManager = exportManager;
			_permissionService = permissionService;
			_sinek_ModelFactory = sinek_ModelFactory;
			_notificationService = notificationService;
		}

		#endregion

		#region Utilities

		protected virtual void UpdateLocales(Sinek_Model sinek_Model, Sinek_ModelModel model)
		{
			foreach (var localized in model.Locales)
			{
				_localizedEntityService.SaveLocalizedValue(sinek_Model,
															   x => x.Name,
															   localized.Name,
															   localized.LanguageId);

				_localizedEntityService.SaveLocalizedValue(sinek_Model,
														   x => x.Description,
														   localized.Description,
														   localized.LanguageId);

				_localizedEntityService.SaveLocalizedValue(sinek_Model,
														   x => x.ManufacturerId,
														   localized.ManufacturerId,
														   localized.LanguageId);


			}
		}

		#endregion

		public IActionResult GetSinek_ModelByManufacturerId(string manufacturerId, bool addEmptyModelIfRequired)
		{
			//manufacturerId = "1";
			// This action method gets called via an ajax request
			if (String.IsNullOrEmpty(manufacturerId))
				throw new ArgumentNullException("manufacturerId");

			var manufacturer = _manufacturerService.GetManufacturerById(Convert.ToInt32(manufacturerId));
			var sinek_Models = manufacturer != null ? _sinek_ModelService.GetSinek_ModelByManufacturerId(manufacturer.Id).ToList() : new List<Sinek_Model>();
			var result = (from s in sinek_Models
						  select new { id = s.Id, name = _localizationService.GetLocalized(s, entity => entity.Name, null, false, false) }).ToList();
			//if (addEmptyModelIfRequired && result.Count == 0)
			//    result.Insert(0, new { id = 0, name = _localizationService.GetResource("Address.OtherNonUS") });
			return Json(result);
		}

		public IActionResult GetSinek_CutPointByManufacturerIdModelId(string modelId, string manufacturerId, bool addEmptyModelIfRequired)
		{
			//manufacturerId = "1";
			// This action method gets called via an ajax request
			if (String.IsNullOrEmpty(manufacturerId) && String.IsNullOrEmpty(modelId))
				throw new ArgumentNullException("modelId");

			var manufacturer = _manufacturerService.GetManufacturerById(Convert.ToInt32(manufacturerId));
			var sinek_Model = _sinek_ModelService.GetSinek_ModelById(Convert.ToInt32(modelId));
			var sinek_CutPoints = manufacturer != null && sinek_Model != null ? _sinek_CutPointService.GetSinek_CutPointByManufacturerIdModelId(sinek_Model.Id, manufacturer.Id).ToList() : new List<Sinek_CutPoint>();
			var result = (from s in sinek_CutPoints
						  select new { id = s.CutPoint, name = _localizationService.GetLocalized(s, entity => entity.Size, null, false, false) }).ToList();
			//if (addEmptyModelIfRequired && result.Count == 0)
			//    result.Insert(0, new { id = 0, name = _localizationService.GetResource("Address.OtherNonUS") });
			return Json(result);
		}

		public IActionResult GetSinek_CutPointByModelIId(string modelId, string manufacturerId, bool addEmptyModelIfRequired)
		{
			//manufacturerId = "1";
			// This action method gets called via an ajax request
			if (String.IsNullOrEmpty(manufacturerId) && String.IsNullOrEmpty(modelId))
				throw new ArgumentNullException("modelId");

			var manufacturer = _manufacturerService.GetManufacturerById(Convert.ToInt32(manufacturerId));
			var sinek_Model = _sinek_ModelService.GetSinek_ModelById(Convert.ToInt32(modelId));
			var sinek_CutPoints = manufacturer != null && sinek_Model != null ? _sinek_CutPointService.GetSinek_CutPointByModelIId(sinek_Model.Id, manufacturer.Id).ToList() : new List<Sinek_CutPoint>();
			var result = (from s in sinek_CutPoints
						  select new { id = s.Id, name = _localizationService.GetLocalized(s, entity => entity.Size, null, false, false) }).ToList();
			//if (addEmptyModelIfRequired && result.Count == 0)
			//    result.Insert(0, new { id = 0, name = _localizationService.GetResource("Address.OtherNonUS") });
			return Json(result);
		}

		public IActionResult GetSinek_CutPointByManufacturerIdModelIdSize(string modelId, string manufacturerId, string size, bool addEmptyModelIfRequired)
		{
			//manufacturerId = "1";
			// This action method gets called via an ajax request
			if (String.IsNullOrEmpty(manufacturerId) && String.IsNullOrEmpty(modelId) && String.IsNullOrEmpty(size))
				throw new ArgumentNullException("modelId");

			var manufacturer = _manufacturerService.GetManufacturerById(Convert.ToInt32(manufacturerId));
			var sinek_Model = _sinek_ModelService.GetSinek_ModelById(Convert.ToInt32(modelId));
			//var size = _sinek_ModelService.GetSinek_ModelById(Convert.ToString(size));
			var sinek_CutPoints = manufacturer != null && sinek_Model != null && size != null ? _sinek_CutPointService.GetSinek_CutPointByManufacturerIdModelIdSize(sinek_Model.Id, manufacturer.Id, size).ToList() : new List<Sinek_CutPoint>();
			var result = (from s in sinek_CutPoints
						  select new { id = s.Id, name = _localizationService.GetLocalized(s, entity => entity.CutPoint, null, false, false) }).ToList();
			//if (addEmptyModelIfRequired && result.Count == 0)
			//    result.Insert(0, new { id = 0, name = _localizationService.GetResource("Address.OtherNonUS") });
			return Json(result);
		}

		#region List

		public IActionResult Index()
		{
			return RedirectToAction("List");
		}

		public IActionResult List()
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekModel))
				return AccessDeniedView();

			//prepare model
			var model = _sinek_ModelFactory.PrepareSinekModelViewSearchModel(new Sinek_Model_ViewSearchModel());

			return View(model);
		}

		[HttpPost]
		public IActionResult List(Sinek_Model_ViewSearchModel searchModel)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekModel))
				return AccessDeniedView();

			//prepare model
			var model = _sinek_ModelFactory.PrepareSinekModelViewListModel(searchModel);

			return Json(model);
		}

		#endregion

		#region Create / Edit / Delete

		public IActionResult Create()
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekModel))
				return AccessDeniedView();

			//prepare model
			var model = _sinek_ModelFactory.PrepareSinekModelModel(new Sinek_ModelModel(), null);

			return View(model);
		}

		[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
		public IActionResult Create(Sinek_ModelModel model, bool continueEditing)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekModel))
				return AccessDeniedView();

			if (model.ManufacturerId == 0)
				ModelState.AddModelError("", "Please provide blade make.");

			if (ModelState.IsValid)
			{
				var sinek_Model = model.ToEntity<Sinek_Model>();
				sinek_Model.CreatedOnUtc = DateTime.UtcNow;
				sinek_Model.UpdatedOnUtc = DateTime.UtcNow;

				_sinek_ModelService.InsertSinek_Model(sinek_Model);

				//locales
				UpdateLocales(sinek_Model, model);

				_notificationService.SuccessNotification(_localizationService.GetResource("Admin.Catalog.Sinek_Models.Added"));
				return continueEditing ? RedirectToAction("Edit", new { id = sinek_Model.Id }) : RedirectToAction("List");
			}

			//prepare model
			model = _sinek_ModelFactory.PrepareSinekModelModel(model, null, true);

			//if we got this far, something failed, redisplay form
			return View(model);
		}

		public IActionResult Edit(int id)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekModel))
				return AccessDeniedView();

			var sinek_Model = _sinek_ModelService.GetSinek_ModelById(id);
			if (sinek_Model == null || sinek_Model.Deleted)
				//No manufacturer found with the specified id
				return RedirectToAction("List");

			//prepare model
			var model = _sinek_ModelFactory.PrepareSinekModelModel(null, sinek_Model);

			return View(model);
		}

		[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
		public IActionResult Edit(Sinek_ModelModel model, bool continueEditing)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekModel))
				return AccessDeniedView();

			var sinek_Model = _sinek_ModelService.GetSinek_ModelById(model.Id);
			if (sinek_Model == null || sinek_Model.Deleted)
				//No manufacturer found with the specified id
				return RedirectToAction("List");

			if (model.ManufacturerId == 0)
				ModelState.AddModelError("", "Please provide blade make.");

			if (ModelState.IsValid)
			{
				sinek_Model = model.ToEntity(sinek_Model);
				sinek_Model.UpdatedOnUtc = DateTime.UtcNow;

				_sinek_ModelService.UpdateSinek_Model(sinek_Model);
				//locales
				UpdateLocales(sinek_Model, model);

				_notificationService.SuccessNotification(_localizationService.GetResource("Admin.Catalog.Sinek_Models.Updated"));
				return continueEditing ? RedirectToAction("Edit", sinek_Model.Id) : RedirectToAction("List");
			}

			//prepare model
			model = _sinek_ModelFactory.PrepareSinekModelModel(model, sinek_Model, true);

			//if we got this far, something failed, redisplay form
			return View(model);
		}

		[HttpPost]
		public virtual IActionResult Delete(int id)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageCategories))
				return AccessDeniedView();

			var sinek_Model = _sinek_ModelService.GetSinek_ModelById(id);
			if (sinek_Model == null || sinek_Model.Deleted)
				//No manufacturer found with the specified id
				return RedirectToAction("List");

			_sinek_ModelService.DeleteSinek_Model(sinek_Model);

			_notificationService.SuccessNotification(_localizationService.GetResource("Admin.Catalog.Sinek_Models.Deleted"));

			return RedirectToAction("List");
		}

		public IActionResult RemoveCurrentSinekModelId(string id)
		{
			string result = "";

			try
			{
				if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekModel))
					return AccessDeniedView();

				var sinek_Model = _sinek_ModelService.GetSinek_ModelById(Convert.ToInt32(id));
				if (sinek_Model == null)
					//No manufacturer found with the specified id
					return RedirectToAction("List");

				_sinek_ModelService.DeleteSinek_Model(sinek_Model);

				//activity log
				//_customerActivityService.InsertActivity("DeleteManufacturer", _localizationService.GetResource("ActivityLog.DeleteManufacturer"), sinek_Model.Name);

				_notificationService.SuccessNotification(_localizationService.GetResource("Admin.Catalog.Sinek_Models.Deleted"));

				result = "Success";
			}
			catch
			{
				result = "Failure";
			}

			return Json(result);
		}

		#endregion

		#region Export / Import

		public IActionResult ExportXml()
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekModel))
				return AccessDeniedView();

			try
			{
				var fileName = string.Format("sinek_Models_{0}.xml", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
				var sinek_Models = _sinek_ModelService.GetAllSinek_Models(true);
				var xml = _exportManager.ExportSinek_ModelsToXml(sinek_Models);

				return File(Encoding.UTF8.GetBytes(xml), "application/xml", fileName);
			}
			catch (Exception exc)
			{
				_notificationService.ErrorNotification(exc);
				return RedirectToAction("List");
			}
		}

		#endregion
	}
}
