﻿using Microsoft.AspNetCore.Mvc;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.NB;
using Nop.Services.ExportImport;
using Nop.Services.Localization;
using Nop.Services.Messages;
using Nop.Services.NB;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Factories.NB;
using Nop.Web.Areas.Admin.Infrastructure.Mapper.Extensions;
using Nop.Web.Areas.Admin.Models.NB;
using Nop.Web.Framework.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;

namespace Nop.Web.Areas.Admin.Controllers.NB
{
	public class Sinek_LocationController : BaseAdminController
	{
		#region Fields

		private readonly ISinek_LocationService _sinek_LocationService;
		private readonly ILanguageService _languageService;
		private readonly ILocalizationService _localizationService;
		private readonly ILocalizedEntityService _localizedEntityService;
		private readonly IExportManager _exportManager;
		private readonly IPermissionService _permissionService;
		private readonly AdminAreaSettings _adminAreaSettings;
		private readonly CatalogSettings _catalogSettings;
		private readonly ISinek_LocationModelFactory _sinek_LocationModelFactory;
		private readonly INotificationService _notificationService;

		#endregion

		#region Ctor

		public Sinek_LocationController(
			ISinek_LocationService sinek_LocationService,
			ILanguageService languageService,
			ILocalizationService localizationService,
			ILocalizedEntityService localizedEntityService,
			IExportManager exportManager,
			IPermissionService permissionService,
			AdminAreaSettings adminAreaSettings,
			CatalogSettings catalogSettings,
			ISinek_LocationModelFactory sinek_LocationModelFactory,
			INotificationService notificationService)
		{
			_sinek_LocationService = sinek_LocationService;
			_languageService = languageService;
			_localizationService = localizationService;
			_localizedEntityService = localizedEntityService;
			_exportManager = exportManager;
			_permissionService = permissionService;
			_adminAreaSettings = adminAreaSettings;
			_catalogSettings = catalogSettings;
			_sinek_LocationModelFactory = sinek_LocationModelFactory;
			_notificationService = notificationService;
		}

		#endregion

		#region Utilities

		protected virtual void UpdateLocales(Sinek_Location sinek_Location, Sinek_LocationModel model)
		{
			foreach (var localized in model.Locales)
			{
				_localizedEntityService.SaveLocalizedValue(sinek_Location,
															   x => x.EMail,
															   localized.EMail,
															   localized.LanguageId);

				_localizedEntityService.SaveLocalizedValue(sinek_Location,
															   x => x.Fax,
															   localized.Fax,
															   localized.LanguageId);

				_localizedEntityService.SaveLocalizedValue(sinek_Location,
															   x => x.LocationAddress,
															   localized.LocationAddress,
															   localized.LanguageId);

				_localizedEntityService.SaveLocalizedValue(sinek_Location,
															   x => x.Name1,
															   localized.Name1,
															   localized.LanguageId);

				_localizedEntityService.SaveLocalizedValue(sinek_Location,
															   x => x.Name2,
															   localized.Name2,
															   localized.LanguageId);

				_localizedEntityService.SaveLocalizedValue(sinek_Location,
															   x => x.Notes,
															   localized.Notes,
															   localized.LanguageId);

				_localizedEntityService.SaveLocalizedValue(sinek_Location,
															   x => x.PhoneNo1,
															   localized.PhoneNo1,
															   localized.LanguageId);

				_localizedEntityService.SaveLocalizedValue(sinek_Location,
															   x => x.PhoneNo2,
															   localized.PhoneNo2,
															   localized.LanguageId);

				_localizedEntityService.SaveLocalizedValue(sinek_Location,
															   x => x.WebSite,
															   localized.WebSite,
															   localized.LanguageId);

				_localizedEntityService.SaveLocalizedValue(sinek_Location,
															   x => x.SellsRockerz,
															   localized.SellsRockerz,
															   localized.LanguageId);

				_localizedEntityService.SaveLocalizedValue(sinek_Location,
															   x => x.SellsSk8tape,
															   localized.SellsSk8tape,
															   localized.LanguageId);

			}
		}

		#endregion

		#region List

		public IActionResult Index()
		{
			return RedirectToAction("List");
		}

		public IActionResult List()
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekLocation))
				return AccessDeniedView();

			//prepare model
			var model = _sinek_LocationModelFactory.PrepareSinekLocationSearchModel(new Sinek_LocationSearchModel());

			return View(model);
		}

		[HttpPost]
		public IActionResult List(Sinek_LocationSearchModel searchModel)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekLocation))
				return AccessDeniedView();

			//prepare model
			var model = _sinek_LocationModelFactory.PrepareSinekLocationListModel(searchModel);

			return Json(model);
		}

		#endregion

		#region Create / Edit / Delete

		public IActionResult Create()
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekLocation))
				return AccessDeniedView();

			//prepare model
			var model = _sinek_LocationModelFactory.PrepareSinekLocationModel(new Sinek_LocationModel(), null);

			return View(model);
		}

		[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
		public IActionResult Create(Sinek_LocationModel model, bool continueEditing)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekLocation))
				return AccessDeniedView();

			#region Validate Address

			//validate address
			if (string.IsNullOrEmpty(model.LocationAddress))
				ModelState.AddModelError("", "Please provide Address");

			string status;
			string locationType = string.Empty;
			double Latitude = 0;
			double Longitude = 0;

			string PostUrl = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + model.LocationAddress.ToString().Trim() + " &sensor=true";
			XDocument geo = XDocument.Load(PostUrl);

			status = Convert.ToString((from geocode in geo.Descendants("GeocodeResponse")
									   select geocode.Element("status")).SingleOrDefault());

			if (status.Equals("<status>OVER_QUERY_LIMIT</status>"))
			{
				Thread.Sleep(50000);
				//GetGeocodingSearchResults(address);
				PostUrl = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + model.LocationAddress.ToString().Trim() + " &sensor=true";

				geo = XDocument.Load(PostUrl);

				status = Convert.ToString((from geocode in geo.Descendants("GeocodeResponse")
										   select geocode.Element("status")).SingleOrDefault());

				locationType = Convert.ToString((from geocode in geo.Descendants("geometry")
												 select geocode.Element("location_type")).First()).Split('>')[1].Split('<')[0].ToString();
			}

			if (status.Equals("<status>OK</status>"))
			{
				var q = (from b in geo.Descendants("location")
						 select new
						 {
							 Lat = (double?)b.Element("lat"),
							 Lng = (double?)b.Element("lng")
						 }).ToList();

				locationType = Convert.ToString((from geocode in geo.Descendants("geometry")
												 select geocode.Element("location_type")).First()).Split('>')[1].Split('<')[0].ToString();

				Latitude = Convert.ToDouble(q[0].Lat);
				Longitude = Convert.ToDouble(q[0].Lng);
			}
			else if (status.Equals("<status>ZERO_RESULTS</status>"))
			{
				ModelState.AddModelError("", "These address [" + model.LocationAddress + "] does not exist in google map. Please give another address");
			}

			#endregion

			if (ModelState.IsValid)
			{
				var sinek_Location = model.ToEntity<Sinek_Location>();

				sinek_Location.CreatedOnUtc = DateTime.UtcNow;
				sinek_Location.UpdatedOnUtc = DateTime.UtcNow;
				sinek_Location.LocationType = locationType;
				sinek_Location.Latitude = Latitude;
				sinek_Location.Longitude = Longitude;

				if (model.Latitude != 0)
				{
					sinek_Location.Latitude = model.Latitude;
				}

				if (model.Longitude != 0)
				{
					sinek_Location.Longitude = model.Longitude;
				}

				//insert
				_sinek_LocationService.InsertSinek_Location(sinek_Location);

				//locales
				UpdateLocales(sinek_Location, model);

				_notificationService.SuccessNotification(_localizationService.GetResource("Admin.Catalog.Sinek_Locations.Added"));
				return continueEditing ? RedirectToAction("Edit", new { id = sinek_Location.Id }) : RedirectToAction("List");
			}

			//prepare model
			model = _sinek_LocationModelFactory.PrepareSinekLocationModel(model, null, true);

			//if we got this far, something failed, redisplay form
			return View(model);
		}

		public IActionResult Edit(int id)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekLocation))
				return AccessDeniedView();

			var sinek_Location = _sinek_LocationService.GetSinek_LocationById(id);
			if (sinek_Location == null || sinek_Location.Deleted)
				return RedirectToAction("List");

			//prepare model
			var model = _sinek_LocationModelFactory.PrepareSinekLocationModel(null, sinek_Location);

			return View(model);
		}

		[HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
		public IActionResult Edit(Sinek_LocationModel model, bool continueEditing)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekLocation))
				return AccessDeniedView();

			var sinek_Location = _sinek_LocationService.GetSinek_LocationById(model.Id);
			if (sinek_Location == null || sinek_Location.Deleted)
				return RedirectToAction("List");

			#region Validate Address

			//validate address
			if (string.IsNullOrEmpty(model.LocationAddress))
				ModelState.AddModelError("", "Please provide Address");

			string status;
			string locationType = string.Empty;
			double Latitude = 0;
			double Longitude = 0;

			string PostUrl = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + model.LocationAddress.ToString().Trim() + " &sensor=true";
			XDocument geo = XDocument.Load(PostUrl);

			status = Convert.ToString((from geocode in geo.Descendants("GeocodeResponse")
									   select geocode.Element("status")).SingleOrDefault());

			if (status.Equals("<status>OVER_QUERY_LIMIT</status>"))
			{
				Thread.Sleep(50000);
				//GetGeocodingSearchResults(address);
				PostUrl = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + model.LocationAddress.ToString().Trim() + " &sensor=true";

				geo = XDocument.Load(PostUrl);

				status = Convert.ToString((from geocode in geo.Descendants("GeocodeResponse")
										   select geocode.Element("status")).SingleOrDefault());

				locationType = Convert.ToString((from geocode in geo.Descendants("geometry")
												 select geocode.Element("location_type")).First()).Split('>')[1].Split('<')[0].ToString();
			}

			if (status.Equals("<status>OK</status>"))
			{
				var q = (from b in geo.Descendants("location")
						 select new
						 {
							 Lat = (double?)b.Element("lat"),
							 Lng = (double?)b.Element("lng")
						 }).ToList();

				locationType = Convert.ToString((from geocode in geo.Descendants("geometry")
												 select geocode.Element("location_type")).First()).Split('>')[1].Split('<')[0].ToString();

				Latitude = Convert.ToDouble(q[0].Lat);
				Longitude = Convert.ToDouble(q[0].Lng);
			}
			else if (status.Equals("<status>ZERO_RESULTS</status>"))
			{
				ModelState.AddModelError("", "These address [" + model.LocationAddress + "] does not exist in google map. Please give another address");
			}

			#endregion

			if (ModelState.IsValid)
			{
				sinek_Location = model.ToEntity(sinek_Location);
				sinek_Location.LocationType = locationType;
				sinek_Location.Latitude = Latitude;
				sinek_Location.Longitude = Longitude;
				sinek_Location.UpdatedOnUtc = DateTime.UtcNow;

				if (model.Latitude != 0)
				{
					sinek_Location.Latitude = model.Latitude;
				}

				if (model.Longitude != 0)
				{
					sinek_Location.Longitude = model.Longitude;
				}

				//update
				_sinek_LocationService.UpdateSinek_Location(sinek_Location);

				//locales
				UpdateLocales(sinek_Location, model);

				_notificationService.SuccessNotification(_localizationService.GetResource("Admin.Catalog.Sinek_Locations.Updated"));
				return continueEditing ? RedirectToAction("Edit", sinek_Location.Id) : RedirectToAction("List");
			}


			//prepare model
			model = _sinek_LocationModelFactory.PrepareSinekLocationModel(model, sinek_Location, true);

			//if we got this far, something failed, redisplay form
			return View(model);
		}

		public IActionResult Delete(int id)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekLocation))
				return AccessDeniedView();

			var sinek_Location = _sinek_LocationService.GetSinek_LocationById(id);
			if (sinek_Location == null)
				return RedirectToAction("List");

			//delete
			_sinek_LocationService.DeleteSinek_Location(sinek_Location);

			_notificationService.SuccessNotification(_localizationService.GetResource("Admin.Catalog.Sinek_Locations.Deleted"));
			return RedirectToAction("List");
		}

		#endregion

		#region Export / Import

		public IActionResult ExportXml()
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekLocation))
				return AccessDeniedView();

			try
			{
				var fileName = string.Format("sinek_Locations_{0}.xml", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
				var sinek_Locations = _sinek_LocationService.GetAllSinek_Locations(true);
				var xml = _exportManager.ExportSinek_LocationsToXml(sinek_Locations);
				return File(Encoding.UTF8.GetBytes(xml), "application/xml", fileName);
			}
			catch (Exception exc)
			{
				_notificationService.ErrorNotification(exc);
				return RedirectToAction("List");
			}
		}

		#endregion

		public IActionResult GetMap(string address)
		{
			string status;

			#region Here we handle ProductVariantAttributeValue

			string PostUrl = "http://maps.googleapis.com/maps/api/geocode/xml?address=" + address.ToString().Trim() + " &sensor=false";

			XDocument geo = XDocument.Load(PostUrl);

			status = Convert.ToString((from geocode in geo.Descendants("GeocodeResponse")
									   select geocode.Element("status")).SingleOrDefault());

			#endregion

			return Json(status);
		}
		public class MapInfo
		{
			public string status { get; set; }
		}

		[HttpPost]
		public IActionResult DeleteSelected(ICollection<int> selectedIds)
		{
			if (!_permissionService.Authorize(StandardPermissionProvider.ManageSinekLocation))
				return AccessDeniedView();

			if (selectedIds != null)
			{
				foreach(var id in selectedIds)
				{
					var sinekLocation = _sinek_LocationService.GetSinek_LocationById(id);
					if (sinekLocation != null)
					{
						//delete
						_sinek_LocationService.DeleteSinek_Location(sinekLocation);
					}
				}
			}

			return Json(new { Result = true });
		}
	}
}
