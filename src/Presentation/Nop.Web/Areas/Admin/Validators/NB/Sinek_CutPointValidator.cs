﻿using FluentValidation;
using Nop.Core.Domain.NB;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Areas.Admin.Models.NB;
using Nop.Web.Framework.Validators;

namespace Nop.Web.Areas.Admin.Validators.NB
{
    public partial class Sinek_CutPointValidator : BaseNopValidator<Sinek_CutPointModel>
    {
        public Sinek_CutPointValidator(ILocalizationService localizationService, IDbContext dbContext)
        {
            RuleFor(x => x.ManufacturerId).NotNull().WithMessage(localizationService.GetResource("Admin.Catalog.Sinek_CutPoint.Fields.ManufacturerId.Required"));
            //RuleFor(x => x.ManufacturerId).Equal(0).WithMessage(localizationService.GetResource("Admin.Catalog.Sinek_CutPoint.Fields.ManufacturerId.Required"));
            RuleFor(x => x.ModelId).NotNull().WithMessage(localizationService.GetResource("Admin.Catalog.Sinek_CutPoint.Fields.ModelId.Required"));
            RuleFor(x => x.Size).NotNull().WithMessage(localizationService.GetResource("Admin.Catalog.Sinek_CutPoint.Fields.Size.Required"));
            RuleFor(x => x.CutPoint).NotNull().WithMessage(localizationService.GetResource("Admin.Catalog.Sinek_CutPoint.Fields.CutPoint.Required"));
            RuleFor(x => x.DisplayOrder).NotNull().WithMessage(localizationService.GetResource("Admin.Catalog.Sinek_CutPoint.Fields.DisplayOrder.Required"));

            SetDatabaseValidationRules<Sinek_CutPoint>(dbContext);
        }
    }
}
