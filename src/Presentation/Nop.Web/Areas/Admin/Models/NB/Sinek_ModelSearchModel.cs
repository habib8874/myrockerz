﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB
{
	/// <summary>
	/// Represents a sinek model search model
	/// </summary>
	public partial class Sinek_ModelSearchModel : BaseSearchModel
    {
        #region Ctor

        public Sinek_ModelSearchModel()
        {
        }

        #endregion

        #region Properties

        #endregion
    }
}
