﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.NB
{
	/// <summary>
	/// Represents a Sinek_CutPoint_View model
	/// </summary>
	public class Sinek_CutPoint_ViewModel : BaseNopEntityModel, ILocalizedModel<Sinek_CutPoint_ViewLocalizedModel>
	{
		#region Ctor

		public Sinek_CutPoint_ViewModel()
		{
			if (PageSize < 1)
			{
				PageSize = 5;
			}
			Locales = new List<Sinek_CutPoint_ViewLocalizedModel>();
			AvailableManufacturerId = new List<SelectListItem>();
			AvailableModelId = new List<SelectListItem>();
		}

		#endregion

		#region Properties

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.ManufacturerId")]
		public int ManufacturerId { get; set; }
		public IList<SelectListItem> AvailableManufacturerId { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.ModelId")]
		public int ModelId { get; set; }
		public IList<SelectListItem> AvailableModelId { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.Size")]
		public string Size { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.CutPoint")]
		public string CutPoint { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.AdditionalInfo")]
		public string AdditionalInfo { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.PageSize")]
		public int PageSize { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.PageSizeOptions")]
		public string PageSizeOptions { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.Published")]
		public bool Published { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.Deleted")]
		public bool Deleted { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.CreatedOnUtc")]
		public DateTime CreatedOnUtc { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.UpdatedOnUtc")]
		public DateTime UpdatedOnUtc { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.ManufacturerName")]
		public string ManufacturerName { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.Sinek_ModelName")]
		public string Sinek_ModelName { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.DisplayOrder")]
		public string DisplayOrder { get; set; }

		public IList<Sinek_CutPoint_ViewLocalizedModel> Locales { get; set; }

		public bool HideNameAndDescriptionProperties { get; set; }
		public bool HidePublishedProperty { get; set; }
		public bool HideDisplayOrderProperty { get; set; }

		#endregion
	}

	public class Sinek_CutPoint_ViewLocalizedModel : ILocalizedLocaleModel
	{
		public int LanguageId { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.Id")]
		public int Id { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.ManufacturerId")]
		public int ManufacturerId { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.ModelId")]
		public int ModelId { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.Size")]
		public string Size { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.CutPoint")]
		public string CutPoint { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.AdditionalInfo")]
		public string AdditionalInfo { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.PageSize")]
		public int PageSize { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.PageSizeOptions")]
		public string PageSizeOptions { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.Published")]
		public bool Published { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.Deleted")]
		public bool Deleted { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.CreatedOnUtc")]
		public DateTime CreatedOnUtc { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.UpdatedOnUtc")]
		public DateTime UpdatedOnUtc { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.ManufacturerName")]
		public string ManufacturerName { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.Sinek_ModelName")]
		public string Sinek_ModelName { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_CutPoint_Views.Fields.DisplayOrder")]
		public string DisplayOrder { get; set; }
	}
}
