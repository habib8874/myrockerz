﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB
{
	/// <summary>
	/// Represents a sinek cutpoint list model
	/// </summary>
	public partial class Sinek_CutPointListModel : BasePagedListModel<Sinek_CutPointSearchModel>
	{
	}
}
