﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.NB
{
	/// <summary>
	/// Represents a SinekModel model
	/// </summary>
	public partial class Sinek_ModelModel : BaseNopEntityModel, ILocalizedModel<Sinek_ModelLocalizedModel>
	{
		#region Ctor

		public Sinek_ModelModel()
		{
			if (PageSize < 1)
			{
				PageSize = 5;
			}
			Locales = new List<Sinek_ModelLocalizedModel>();
			AvailableManufacturerId = new List<SelectListItem>();
		}

		#endregion

		#region Properties

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.Name")]
		public string Name { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.Description")]
		public string Description { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.PageSize")]
		public int PageSize { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.PageSizeOptions")]
		public string PageSizeOptions { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.Published")]
		public bool Published { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.Deleted")]
		public bool Deleted { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.CreatedOnUtc")]
		public DateTime CreatedOnUtc { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.UpdatedOnUtc")]
		public DateTime UpdatedOnUtc { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.ManufacturerId")]
		public int ManufacturerId { get; set; }
		public IList<SelectListItem> AvailableManufacturerId { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.DisplayOrder")]
		public int DisplayOrder { get; set; }


		public IList<Sinek_ModelLocalizedModel> Locales { get; set; }

		public bool HideNameAndDescriptionProperties { get; set; }
		public bool HidePublishedProperty { get; set; }
		public bool HideDisplayOrderProperty { get; set; }

		#endregion
	}

	public partial class Sinek_ModelLocalizedModel : ILocalizedLocaleModel
	{
		public int LanguageId { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.Name")]
		public string Name { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.Description")]
		public string Description { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.PageSize")]
		public int PageSize { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.PageSizeOptions")]
		public string PageSizeOptions { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.Published")]
		public bool Published { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.Deleted")]
		public bool Deleted { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.CreatedOnUtc")]
		public DateTime CreatedOnUtc { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.UpdatedOnUtc")]
		public DateTime UpdatedOnUtc { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.ManufacturerId")]
		public int ManufacturerId { get; set; }

		[NopResourceDisplayName("Admin.Catalog.Sinek_Models.Fields.DisplayOrder")]
		public DateTime DisplayOrder { get; set; }
	}
}
