﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

namespace Nop.Web.Areas.Admin.Models.NB
{
    /// <summary>
    /// Represents a sinek model view search model
    /// </summary>
    public partial class Sinek_Model_ViewSearchModel : BaseSearchModel
    {
        #region Ctor

        public Sinek_Model_ViewSearchModel()
        {
            AvailableManufacturers = new List<SelectListItem>();
        }

        #endregion

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Sinek_Models.Search.Fields.Name")]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.Catalog.Sinek_Models.Search.Fields.Manufacturer")]
        public int ManufacturerId { get; set; }
        public IList<SelectListItem> AvailableManufacturers { get; set; }

        #endregion
    }
}
