﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB
{
	/// <summary>
	/// Represents a sinek model view list model
	/// </summary>
	public partial class Sinek_Model_ViewListModel : BasePagedListModel<Sinek_Model_ViewModel>
	{
	}
}
