﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB
{
	/// <summary>
	/// Represents a sinek location list model
	/// </summary>
	public partial class Sinek_LocationListModel : BasePagedListModel<Sinek_LocationModel>
	{
	}
}
