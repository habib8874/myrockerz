﻿using Nop.Web.Framework.Models;

namespace Nop.Web.Areas.Admin.Models.NB
{
	/// <summary>
	/// Represents a sinek cutpoint view list model
	/// </summary>
	public partial class Sinek_CutPoint_ViewListModel : BasePagedListModel<Sinek_CutPoint_ViewModel>
	{
	}
}
