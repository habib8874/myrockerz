﻿/// <reference path="~/Scripts/jquery-1.7.1-vsdoc.js" />
/// <reference path="~/Scripts/rockerz.common.js" />

$(document).ready(function () {
    $("#company_tabs").tabs();
    var whichtab = getParameterByName("t");
    if (whichtab === "c") {
        $("#company_tabs").tabs("option", "selected", 1);
    }
});

function leftNavActive() {

var url = this.location.href;
var activePage = url.substring(url.lastIndexOf('/') + 1);
$('.block ul.list.list2 a').each(function () {
    var currentPage = this.href.substring(this.href.lastIndexOf('/') + 1);
    $(this).removeClass('active');
    if (activePage == currentPage) {
        if (!$(this).hasClass('active')) {
            $(".block ul.list.list2 a.active").removeClass("active");
            $(this).addClass("active");
        }
    }
});
}