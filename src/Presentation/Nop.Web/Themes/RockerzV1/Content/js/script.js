
function leftNavActive() {
	var url = this.location.href;
	var activePage = url.substring(url.lastIndexOf('/') + 1);
	$('.block ul.list.list2 a').each(function () {
		var currentPage = this.href.substring(this.href.lastIndexOf('/') + 1);
		$(this).removeClass('active');
		if (activePage == currentPage) {
			if (!$(this).hasClass('active')) {
				$(".block ul.list.list2 a.active").removeClass("active");
				$(this).addClass("active");
			}
		}
	});
}


(function () {

	var stickyTopSections = $('.checkOutC1');

	var stickyTop = function () {
		var scrollTop = $(window).scrollTop();
		$('.checkOutC1').each(function () {
			var $this = $(this);
			if (scrollTop > 200 && scrollTop < 500) {
				$this.addClass('sticky');
			}
			else {
				$this.removeClass('sticky');
			}
		});
	};

	stickyTop();

	$(window).scroll(function () {
		stickyTop();
	});

});

function addressTypeNav() {
	$(".address_type ul li").first().addClass('active');
	$('[name=addressType]').click(function () {
		var value = $(this).val();
		$('[name=addressType]').removeClass('active');
		$(this).addClass('active');
		$('#@Html.IdFor(x=>x.AddressTypeId)').val(value);
		$(this).parents('li').addClass('active').siblings().removeClass('active');
	});
}

function attributeSelector() {
	var springColorSelector = '.springcontrol .spring-color-box';
	$(springColorSelector).on('click', function () {
		$(springColorSelector).removeClass('active');
		$(this).addClass('active');
	});
	var randomizeSelector = '.randomcontrol';
	$(randomizeSelector).on('click', function () {
		$(randomizeSelector).removeClass('active');
		$(this).addClass('active');
	});
	var colorSelector = '.colorcontrol .colors';
	$(colorSelector).on('click', function () {
		$(colorSelector).removeClass('active');
		$(this).addClass('active');
	});
}

function onlyNumberKey(evt) {
	// Only ASCII charactar in that range allowed
	var ASCIICode = (evt.which) ? evt.which : evt.keyCode
	if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
		return false;
	return true;
}
function alphaOnly(event) {
	var key = event.keyCode;
	return ((key > 64 && key < 91) || (key > 96 && event.charCode < 123) || (key == 32));
};



function miniCartMobileC() {
	if (window.innerWidth < 992) {
		$(".checkOutSection").addClass("checkMiniCartMobileOpen");
	} else {
		$(".checkOutSection").removeClass("checkMiniCartMobileOpen");
	}
}
miniCartMobileC();

if (window.location.href === window.location.origin + "/login/checkoutasguest?returnUrl=%2Fcart") {
	window.location.href = window.location.origin + "/onepagecheckout"
}

function DeviceTopicons() {
	if (window.innerWidth < 992) {
		//$(".mobileMenuItem").animate({ "bottom": "60px" });
		setTimeout(function () {
			$(".mobileMenuItem").animate({ "bottom": "80px" });
		}, 500);

		$(".mobileMenuItem").click(function () {
			$(".leftSideCategoryL").addClass("activeMo");
			$(".machentCategoryList").toggleClass("activeLm");
			$(".menuOverLay").toggleClass("activeOL");
			$("body").toggleClass("menuActiveM");
			if ($("body .menuOverLay").length === 0) {
				$(".leftSideCategoryL").prepend('<div class="menuOverLay"></div>');
			}
		});
		$(document).on("click", ".leftBoxMerchant .menuOverLay", function () {
			$(".leftSideCategoryL").removeClass("activeMo");
			$(".machentCategoryList").removeClass("activeLm");
			$("body").removeClass("menuActiveM");
			$(this).remove();
		});
		$(document).on("click", ".machentCategoryList li a", function () {
			$(".leftSideCategoryL").removeClass("activeMo");
			$(".machentCategoryList").removeClass("activeLm");
			$("body").removeClass("menuActiveM");
			$(".leftBoxMerchant .menuOverLay").remove();
		});
		$(document).on("click", ".checkOutSection .checkOutC1", function () {
			if ($(".checkOutSection .catOpt").length === 0) {
				$(this).parent(".checkOutSection").prepend('<div class="catOpt"></div>');
			}
			$("body").addClass("deviceCartActive");
			$(this).parent(".checkOutSection").addClass("catOpenDevic");
			$("header").addClass("headerHide");
			$(this).parent(".checkOutSection").removeClass("checkMiniCartMobileOpen");
		});
		$(document).on("click", ".catOpt", function () {
			$('body').removeClass("deviceCartActive");
			$(".checkOutSection").removeClass("catOpenDevic");
			$("header").removeClass("headerHide");
			$(".checkOutSection").addClass("checkMiniCartMobileOpen");
			$('.tooltripInfo').removeClass('active');
			$('.customToolTripBox').slideUp();
			$(this).remove();
		});

		/*$(".marchentTopFilter h2").each(function(){
			var bdcat = $(this).attr("cccatid");
			var ctVl = $(this).next(".numberOfItems").find("span").html();
			$(".machentCategoryList li a").each(function(){
				var ccidlDv = $(this).attr("idl");
				if(bdcat == ccidlDv){
					$(this).append('<span>'+ctVl+'</span>')
				}
			});
		}); */

		if ($("body .rightUsersDetails").length === 0) {
			$(".restaurInfo").prepend('<div class="rightUsersDetails"><div class="searchIconTopM"></div><div class="deviceUserIcon"></div><div class="deviceUserPr"></div>');
			if ($("body .logoutTxt").length === 0) {
				$(".deviceUserPr").append('<ul><li><a href="#" target="_blank">Contact us</a></li><li><a href="/agentregister">Agent Register</a></li><li><a href="/register">Register</a></li><li><a href="/login">Log in</a></li></ul>');
			} else {
				$(".deviceUserPr").append('<ul><li><a href="#" target="_blank">Contact us</a></li><li class="userNa">' + $(".navbar-nav .custNam").html() + '</li><li><a href="/logout">Log out</a></li></ul>');
			}
		}

		$(window).ajaxComplete(function () {
			$('.searchFilterLeft .backBtnSeach').remove();
			$('.searchFilterLeft input').animate({ "width": "0px", "padding": "0px", "right": "0" });
			$('.searchAndTags').next('.backBtnSeach').remove();
			$('.searchAndTags + .searchOverLay').remove();
		});

	} else {
		$(".rightUsersDetails").remove();
	}
}
DeviceTopicons();
$(window).resize(function () {
	DeviceTopicons();
});


//override global ajax loader
function displayAjaxLoading(display) {
	if (display) {
		$('.ajax-loading-block-window').show();
		$('.ajax-overlay-window').show();
	}
	else {
		$('.ajax-loading-block-window').fadeOut();
		$('.ajax-overlay-window').fadeOut();
	}
}
