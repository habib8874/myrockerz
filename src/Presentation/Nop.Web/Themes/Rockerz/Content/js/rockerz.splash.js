﻿/// <reference path="~/Scripts/jquery-1.7.1-vsdoc.js" />

$(document).ready(function () {
    if ($('.slideshow').length > 0) {
        $('.slideshow').cycle({ fx: 'fade', timeout: 2500, speed: 1000 });
    }
    $("#splash_main").click(function () {
        window.location.href = '/t/Product';
    });
    random_dancers();
});
function random_dancers() {
    var arrBg = ["/Content/Images/DancersCollage_Beata.png", "/Content/Images/DancersCollage_Page.png"];
    var arrHgt = [439, 309];
    var arrMgn = [0, 40];
    var idx = Math.floor(Math.random() * arrBg.length);
    var bg = arrBg[idx];
    var hgt = arrHgt[idx];
    var mgn = arrMgn[idx];
    $("#splash_dancers").css("background", "url(" + bg + ")");
    $("#splash_dancers").css("height", hgt);
    $("#splash_dancers").css("margin-top", mgn);
}
