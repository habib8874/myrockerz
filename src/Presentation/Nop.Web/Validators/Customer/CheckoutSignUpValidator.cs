﻿using FluentValidation;
using Nop.Core.Domain.Customers;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;
using Nop.Web.Models.Checkout;

namespace Nop.Web.Validators.Customer
{
	public class CheckoutSignUpValidator : BaseNopValidator<SignUpModel>
	{
		public CheckoutSignUpValidator(ILocalizationService localizationService,
			CustomerSettings customerSettings)
		{
			RuleFor(x => x.FirstName).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.FirstName.Required"));
			RuleFor(x => x.LastName).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.LastName.Required"));

			RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Email.Required"));
			RuleFor(x => x.Email).EmailAddress().WithMessage(localizationService.GetResource("Common.WrongEmail"));

			//Password rule
			RuleFor(x => x.Password).IsPassword(localizationService, customerSettings);

			if (customerSettings.PhoneEnabled)
			{
				RuleFor(x => x.Phone).NotEmpty().WithMessage(localizationService.GetResource("Account.Fields.Phone.Required"));
			}
		}
	}
}
