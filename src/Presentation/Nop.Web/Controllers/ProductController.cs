﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Security;
using Nop.Core.Domain.Tax;
using Nop.Core.Rss;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Directory;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Web.Areas.Admin.Infrastructure.Cache;
using Nop.Web.Factories;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Mvc;
using Nop.Web.Framework.Mvc.Filters;
using Nop.Web.Framework.Security;
using Nop.Web.Framework.Security.Captcha;
using Nop.Web.Framework.Themes;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Checkout;

namespace Nop.Web.Controllers
{
	public partial class ProductController : BasePublicController
	{
		#region Fields

		private readonly CaptchaSettings _captchaSettings;
		private readonly CatalogSettings _catalogSettings;
		private readonly IAclService _aclService;
		private readonly ICompareProductsService _compareProductsService;
		private readonly ICustomerActivityService _customerActivityService;
		private readonly IEventPublisher _eventPublisher;
		private readonly ILocalizationService _localizationService;
		private readonly IOrderService _orderService;
		private readonly IPermissionService _permissionService;
		private readonly IProductModelFactory _productModelFactory;
		private readonly IProductService _productService;
		private readonly IRecentlyViewedProductsService _recentlyViewedProductsService;
		private readonly IShoppingCartService _shoppingCartService;
		private readonly IStoreContext _storeContext;
		private readonly IStoreMappingService _storeMappingService;
		private readonly IUrlRecordService _urlRecordService;
		private readonly IWebHelper _webHelper;
		private readonly IWorkContext _workContext;
		private readonly IWorkflowMessageService _workflowMessageService;
		private readonly LocalizationSettings _localizationSettings;
		private readonly ShoppingCartSettings _shoppingCartSettings;
		private readonly IProductAttributeService _productAttributeService;
		private readonly ISettingService _settingService;
		private readonly IStaticCacheManager _cacheManager;
		private readonly ILogger _logger;
		private readonly ICurrencyService _currencyService;
		private readonly IProductAttributeParser _productAttributeParser;
		private readonly INotificationService _notificationService;
		private readonly IThemeContext _themeContext;
		private readonly IPriceFormatter _priceFormatter;
		private readonly IPictureService _pictureService;
		private readonly IGenericAttributeService _genericAttributeService;
		private readonly IProductAttributeFormatter _productAttributeFormatter;

		#endregion

		#region Ctor

		public ProductController(CaptchaSettings captchaSettings,
			CatalogSettings catalogSettings,
			IAclService aclService,
			ICompareProductsService compareProductsService,
			ICustomerActivityService customerActivityService,
			IEventPublisher eventPublisher,
			ILocalizationService localizationService,
			IOrderService orderService,
			IPermissionService permissionService,
			IProductModelFactory productModelFactory,
			IProductService productService,
			IRecentlyViewedProductsService recentlyViewedProductsService,
			IShoppingCartService shoppingCartService,
			IStoreContext storeContext,
			IStoreMappingService storeMappingService,
			IUrlRecordService urlRecordService,
			IWebHelper webHelper,
			IWorkContext workContext,
			IWorkflowMessageService workflowMessageService,
			LocalizationSettings localizationSettings,
			ShoppingCartSettings shoppingCartSettings,
			IProductAttributeService productAttributeService,
			ISettingService settingService,
			IStaticCacheManager cacheManager,
			ILogger logger,
			ICurrencyService currencyService,
			IProductAttributeParser productAttributeParser,
			INotificationService notificationService,
			IThemeContext themeContext,
			IPriceFormatter priceFormatter,
			IPictureService pictureService,
			IGenericAttributeService genericAttributeService,
			IProductAttributeFormatter productAttributeFormatter)
		{
			_captchaSettings = captchaSettings;
			_catalogSettings = catalogSettings;
			_aclService = aclService;
			_compareProductsService = compareProductsService;
			_customerActivityService = customerActivityService;
			_eventPublisher = eventPublisher;
			_localizationService = localizationService;
			_orderService = orderService;
			_permissionService = permissionService;
			_productModelFactory = productModelFactory;
			_productService = productService;
			_recentlyViewedProductsService = recentlyViewedProductsService;
			_shoppingCartService = shoppingCartService;
			_storeContext = storeContext;
			_storeMappingService = storeMappingService;
			_urlRecordService = urlRecordService;
			_webHelper = webHelper;
			_workContext = workContext;
			_workflowMessageService = workflowMessageService;
			_localizationSettings = localizationSettings;
			_shoppingCartSettings = shoppingCartSettings;
			_productAttributeService = productAttributeService;
			_settingService = settingService;
			_cacheManager = cacheManager;
			_logger = logger;
			_currencyService = currencyService;
			_productAttributeParser = productAttributeParser;
			_notificationService = notificationService;
			_themeContext = themeContext;
			_priceFormatter = priceFormatter;
			_pictureService = pictureService;
			_genericAttributeService = genericAttributeService;
			_productAttributeFormatter = productAttributeFormatter;
		}

		#endregion

		#region Product details page

		[HttpsRequirement(SslRequirement.No)]
		public virtual IActionResult ProductDetails(int productId, int updatecartitemid = 0)
		{
			var product = _productService.GetProductById(productId);
			if (product == null || product.Deleted)
				return InvokeHttp404();

			var notAvailable =
				//published?
				(!product.Published && !_catalogSettings.AllowViewUnpublishedProductPage) ||
				//ACL (access control list) 
				!_aclService.Authorize(product) ||
				//Store mapping
				!_storeMappingService.Authorize(product) ||
				//availability dates
				!_productService.ProductIsAvailable(product);
			//Check whether the current user has a "Manage products" permission (usually a store owner)
			//We should allows him (her) to use "Preview" functionality
			var hasAdminAccess = _permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel) && _permissionService.Authorize(StandardPermissionProvider.ManageProducts);
			if (notAvailable && !hasAdminAccess)
				return InvokeHttp404();

			//visible individually?
			if (!product.VisibleIndividually)
			{
				//is this one an associated products?
				var parentGroupedProduct = _productService.GetProductById(product.ParentGroupedProductId);
				if (parentGroupedProduct == null)
					return RedirectToRoute("Homepage");

				return RedirectToRoute("Product", new { SeName = _urlRecordService.GetSeName(parentGroupedProduct) });
			}

			//update existing shopping cart or wishlist  item?
			ShoppingCartItem updatecartitem = null;
			if (_shoppingCartSettings.AllowCartItemEditing && updatecartitemid > 0)
			{
				var cart = _shoppingCartService.GetShoppingCart(_workContext.CurrentCustomer, storeId: _storeContext.CurrentStore.Id);
				updatecartitem = cart.FirstOrDefault(x => x.Id == updatecartitemid);
				//not found?
				if (updatecartitem == null)
				{
					return RedirectToRoute("Product", new { SeName = _urlRecordService.GetSeName(product) });
				}
				//is it this product?
				if (product.Id != updatecartitem.ProductId)
				{
					return RedirectToRoute("Product", new { SeName = _urlRecordService.GetSeName(product) });
				}
			}

			//save as recently viewed
			_recentlyViewedProductsService.AddProductToRecentlyViewedList(product.Id);

			//display "edit" (manage) link
			if (_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel) &&
				_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
			{
				//a vendor should have access only to his products
				if (_workContext.CurrentVendor == null || _workContext.CurrentVendor.Id == product.VendorId)
				{
					DisplayEditLink(Url.Action("Edit", "Product", new { id = product.Id, area = AreaNames.Admin }));
				}
			}

			//activity log
			_customerActivityService.InsertActivity("PublicStore.ViewProduct",
				string.Format(_localizationService.GetResource("ActivityLog.PublicStore.ViewProduct"), product.Name), product);

			//model
			var model = _productModelFactory.PrepareProductDetailsModel(product, updatecartitem, false);
			//template
			var productTemplateViewPath = _productModelFactory.PrepareProductTemplateViewPath(product);

			return View(productTemplateViewPath, model);
		}

		#region Custom

		//Code for upgradation
		public IActionResult GetProductVariantAttributeValueId(int ModelIdInt)
		{
			//need to update
			var getproductvariantattributevalue = _productAttributeService.GetProductVariantAttributeValueByModelId(ModelIdInt);

			string getid = "";
			if (getproductvariantattributevalue != null)
				getid = Convert.ToString(getproductvariantattributevalue.Id);
			else
				getid = "";

			return Json(getid);
		}

		public string Bindcolors(int? productId)
		{
			StringBuilder Result = new StringBuilder();
			Product product = _productService.GetProductById(Convert.ToInt32(productId));
			DateTime timeStartMethod = DateTime.Now;

			var model = new ProductDetailsModel()
			{
				Id = product.Id,
				Name = _localizationService.GetLocalized(product, x => x.Name),
				ShortDescription = _localizationService.GetLocalized(product, x => x.ShortDescription),
				FullDescription = _localizationService.GetLocalized(product, x => x.FullDescription),
				MetaKeywords = _localizationService.GetLocalized(product, x => x.MetaKeywords),
				MetaDescription = _localizationService.GetLocalized(product, x => x.MetaDescription),
				MetaTitle = _localizationService.GetLocalized(product, x => x.MetaTitle),
				SeName = _urlRecordService.GetSeName(product),
			};

			//need to update
			//var prdVarient = _productService.GetProductVariantsByProductId(product.Id);
			var variant = product;

			//foreach (var variant in prdVarient)
			// {
			//Update by raman on 21.06.13
			int IntValue = 0;
			IntValue = _settingService.GetSettingByKey("adminareasettings.ProductChange", 0);
			if (IntValue == 1)
			{
				_cacheManager.RemoveByPrefix(NopModelCacheDefaults.MANUFACTURER_NAVIGATION_MODELProduct_KEY);
				_settingService.SetSetting<int>("adminareasettings.ProductChange", 0);
			}
			var cacheKey = string.Format(NopModelCacheDefaults.MANUFACTURER_NAVIGATION_MODELProduct_KEY);
			//var cacheKey = string.Format(PRODUCTDETAIL_BY_ID_KEY, product.Id);

			DateTime timeStartPrepareModel = DateTime.Now;
			var cacheModel = _cacheManager.Get(cacheKey, () =>
			{
				return _productModelFactory.PrepareProductDetailsModel(variant, null, true);
			});

			DateTime timeEndPrepareModel = DateTime.Now;
			double milDiff1 = timeEndPrepareModel.Subtract(timeStartPrepareModel).TotalMilliseconds;
			double secondDiff1 = timeEndPrepareModel.Subtract(timeStartPrepareModel).TotalSeconds;
			string log = String.Format("ParepareModel  Method Query Time in Millisecond is {0} , SEconds is {1} at time {2}", milDiff1, secondDiff1, DateTime.UtcNow.ToString());
			_logger.Information(log);
			model = cacheModel;
			//}
			var defaultProductVariant = model;

			var GuardColors = defaultProductVariant.ProductAttributes.ToList().FirstOrDefault(a => a.Name == "Guard Colors");
			var Springs = defaultProductVariant.ProductAttributes.ToList().FirstOrDefault(a => a.Name == "Spring Set");
			string GoldSpringSetting = "True";
			string SilverSpringSetting = "True";
			foreach (var item in Springs.Values)
			{
				if (item.Name == "Gold")
				{
					if (item.EnableSpring == null || item.EnableSpring == false)
					{
						GoldSpringSetting = "False";
					}
				}
				else if (item.Name == "Silver")
				{
					if (item.EnableSpring == null || item.EnableSpring == false)
					{
						SilverSpringSetting = "False";
					}
				}
			}
			Result.Append(GoldSpringSetting + "`" + SilverSpringSetting);//first assign

			var Color1index = 0;
			var Color2index = 0;
			var Color3index = 0;
			var Color4index = 0;

			StringBuilder HtmlGuardColors = new StringBuilder();//need to be change in string builder
			StringBuilder HtmlGuardColors3 = new StringBuilder();//need to be change in string builder
			StringBuilder HtmlGuardColors2 = new StringBuilder();//need to be change in string builder
			StringBuilder HtmlGuardColors4 = new StringBuilder();//need to be change in string builder
			foreach (var item in GuardColors.Values)
			{
				Color1index = Color1index + 1;
				HtmlGuardColors.Append("<div id='color1_" + item.Id + "' color='" + item.Name.ToString().Replace(" ", "_") + "'  class ='colors' style='" + getStyle(item.ColorCode, item.BorderColorCode) + "'></div>");

				Color3index = Color3index + 1;
				HtmlGuardColors3.Append("<div id='color3_" + item.Id + "' color='" + item.Name.ToString().Replace(" ", "_") + "'  class ='colors' style='" + getStyle(item.ColorCode, item.BorderColorCode) + "'></div>");

				Color2index = Color2index + 1;
				HtmlGuardColors2.Append("<div id='color2_" + item.Id + "' color='" + item.Name.ToString().Replace(" ", "_") + "'  class ='colors' style='" + getStyle(item.ColorCode, item.BorderColorCode) + "'></div>");

				Color4index = Color4index + 1;
				HtmlGuardColors4.Append("<div id='color4_" + item.Id + "' color='" + item.Name.ToString().Replace(" ", "_") + "'  class ='colors' style='" + getStyle(item.ColorCode, item.BorderColorCode) + "'></div>");

			}
			HtmlGuardColors.Append("<br class='clear' />");
			HtmlGuardColors.Append("<div id='rock_back_left_colorname' class='colorname'><span class='colorname'> </span></div>");
			HtmlGuardColors.Append("<span class='colorno'");
			if (Color1index <= 9)
			{
				HtmlGuardColors.Append(" style='position:absolute; left:-60px; top:36px;'");
			}
			HtmlGuardColors.Append(">COLOR 1:</span>");

			HtmlGuardColors3.Append("<br class='clear' />");
			HtmlGuardColors3.Append("<div id='rock_front_left_colorname' class='colorname'><span class='colorname'> </span></div>");
			HtmlGuardColors3.Append("<span class='colorno' style='color:#000!important;'");

			HtmlGuardColors3.Append("<span class='colorno'");
			if (Color3index <= 9)
			{
				HtmlGuardColors3.Append(" style='position:absolute; left:-60px; top:36px;'");
			}
			HtmlGuardColors3.Append(">COLOR 3:</span>");

			HtmlGuardColors2.Append("<br class='clear' />");
			HtmlGuardColors2.Append("<div id='rock_back_right_colorname' class='colorname'><span style='color: #b30f21;'> </span></div>");
			HtmlGuardColors2.Append("<span class='colorno'");

			HtmlGuardColors2.Append("<span class='colorno'");
			if (Color2index <= 9)
			{
				HtmlGuardColors2.Append(" style='position:absolute; left:-60px; top:36px;'");
			}
			HtmlGuardColors2.Append(">COLOR 2:</span>");


			HtmlGuardColors4.Append("<br class='clear' />");
			HtmlGuardColors4.Append("<div id='rock_front_right_colorname' class='colorname'><span style='color: #eedb43;'> </span></div>");
			HtmlGuardColors4.Append("<span class='colorno'");

			HtmlGuardColors4.Append("<span class='colorno'");
			if (Color4index <= 9)
			{
				HtmlGuardColors4.Append(" style='position:absolute; left:-60px; top:36px;'");
			}
			HtmlGuardColors4.Append(">COLOR 4:</span>");
			//string StrRenderPartialViewToString = RenderRazorViewToString("GetDivContent", model);
			string StrRenderPartialViewToString = RenderPartialViewToString("GetDivContent", model);
			Result.Append("`" + HtmlGuardColors + "`" + HtmlGuardColors3 + "`" + HtmlGuardColors2 + "`" + HtmlGuardColors4 + "`" + StrRenderPartialViewToString + "`" + defaultProductVariant.Id);//first assign
			DateTime TimeEndMethod = DateTime.Now;
			double milDiff = TimeEndMethod.Subtract(timeStartMethod).TotalMilliseconds;
			double secondDiff = TimeEndMethod.Subtract(timeStartMethod).TotalSeconds;
			string logOuter = String.Format("BindColor Method Query Time in Millisecond is {0} , SEconds is {1} at time {2}", milDiff, secondDiff, DateTime.UtcNow.ToString());
			_logger.Information(logOuter);
			return Result.ToString();
		}

		//protected string RenderPartialViewToString(string viewName, object model)
		//{
		//    if (string.IsNullOrEmpty(viewName))
		//        viewName = ControllerContext.RouteData.GetRequiredString("action");

		//    ViewData.Model = model;

		//    using (StringWriter sw = new StringWriter())
		//    {
		//        ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
		//        ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
		//        viewResult.View.Render(viewContext, sw);

		//        return sw.GetStringBuilder().ToString();
		//    }
		//}

		//public string RenderRazorViewToString(string viewName, object model)
		//{
		//    ViewData.Model = model;
		//    using (var sw = new StringWriter())
		//    {
		//        var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
		//                                                                 viewName);
		//        var viewContext = new ViewContext(ControllerContext, viewResult.View,
		//                                     ViewData, TempData, sw);
		//        viewResult.View.Render(viewContext, sw);
		//        viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
		//        return sw.GetStringBuilder().ToString();
		//    }
		//}

		public IActionResult GetDivContent(ProductDetailsModel ProductModel)
		{
			return View(ProductModel);
		}

		private string getStyle(string colorCode, string borderColorCode)
		{
			string result = "";
			//result = "float: left; height: 16px; margin: 2px; white-space: nowrap; width: 16px; ";
			if (colorCode != "")
				result += "background-color: " + colorCode + "; ";
			if (borderColorCode != null && borderColorCode != "")
				result += "border: 1px solid " + borderColorCode + ";";
			else
				result += "border: 1px solid " + colorCode + ";";
			return result;
		}


		//add product variant to cart using HTTP POST
		//currently we use this method only for mobile device version
		//desktop version uses AJAX version of this method (ShoppingCartController.AddProductVariantToCart)
		[HttpPost, ActionName("ProductDetails")]
		public IActionResult AddProductVariantToCart(int productId, IFormCollection form)
		{
			var product = _productService.GetProductById(productId);
			if (product == null || product.Deleted || !product.Published)
				return RedirectToRoute("HomePage");

			//manually process form
			int productVariantId = 13;
			ShoppingCartType cartType = ShoppingCartType.ShoppingCart;
			foreach (string formKey in form.Keys)
			{
				if (formKey.StartsWith("addtocartbutton-"))
				{
					productVariantId = Convert.ToInt32(formKey.Substring(("addtocartbutton-").Length));
					cartType = ShoppingCartType.ShoppingCart;
				}
				else if (formKey.StartsWith("addtowishlistbutton-"))
				{
					productVariantId = Convert.ToInt32(formKey.Substring(("addtowishlistbutton-").Length));
					cartType = ShoppingCartType.Wishlist;
				}
			}

			var productVariant = _productService.GetProductById(productVariantId);
			if (productVariant == null)
				return RedirectToRoute("HomePage");

			#region Customer entered price
			decimal customerEnteredPrice = decimal.Zero;
			decimal customerEnteredPriceConverted = decimal.Zero;
			if (productVariant.CustomerEntersPrice)
			{
				foreach (string formKey in form.Keys)
					if (formKey.Equals(string.Format("addtocart_{0}.CustomerEnteredPrice", productVariantId), StringComparison.InvariantCultureIgnoreCase))
					{
						if (decimal.TryParse(form[formKey], out customerEnteredPrice))
							customerEnteredPriceConverted = _currencyService.ConvertToPrimaryStoreCurrency(customerEnteredPrice, _workContext.WorkingCurrency);
						break;
					}
			}
			#endregion

			#region Quantity

			int quantity = 1;
			foreach (string formKey in form.Keys)
				if (formKey.Equals(string.Format("addtocart_{0}.EnteredQuantity", productVariantId), StringComparison.InvariantCultureIgnoreCase))
				{
					int.TryParse(form[formKey], out quantity);
					break;
				}

			#endregion

			var addToCartWarnings = new List<string>();
			string attributes = "";

			#region Product attributes
			string selectedAttributes = string.Empty;
			var productVariantAttributes = _productAttributeService.GetProductAttributeMappingsByProductId(productVariant.Id);
			foreach (var attribute in productVariantAttributes)
			{
				//string controlId = string.Format("product_attribute_{0}_{1}_{2}", attribute.ProductId, attribute.ProductAttributeId, attribute.Id);
				var controlId = $"{NopAttributePrefixDefaults.Product}{attribute.Id}";
				switch (attribute.AttributeControlType)
				{
					case AttributeControlType.DropdownList:
						{
							var ddlAttributes = form[controlId];
							if (!String.IsNullOrEmpty(ddlAttributes))
							{
								int selectedAttributeId = int.Parse(ddlAttributes);
								if (selectedAttributeId > 0)
									selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
										attribute, selectedAttributeId.ToString());
							}
						}
						break;
					case AttributeControlType.RadioList:
						{
							var rblAttributes = form[controlId];
							if (!String.IsNullOrEmpty(rblAttributes))
							{
								int selectedAttributeId = int.Parse(rblAttributes);
								if (selectedAttributeId > 0)
									selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
										attribute, selectedAttributeId.ToString());
							}
						}
						break;
					case AttributeControlType.Checkboxes:
						{
							var cblAttributes = form[controlId];
							if (!String.IsNullOrEmpty(cblAttributes))
							{
								foreach (var item in cblAttributes.ToString()
									.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
								{
									int selectedAttributeId = int.Parse(item);
									if (selectedAttributeId > 0)
										selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
											attribute, selectedAttributeId.ToString());
								}
							}
						}
						break;
					case AttributeControlType.TextBox:
						{
							var txtAttribute = form[controlId];
							if (!String.IsNullOrEmpty(txtAttribute))
							{
								string enteredText = txtAttribute.ToString().Trim();
								selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
									attribute, enteredText);
							}
						}
						break;
					case AttributeControlType.MultilineTextbox:
						{
							var txtAttribute = form[controlId];
							if (!String.IsNullOrEmpty(txtAttribute))
							{
								string enteredText = txtAttribute.ToString().Trim();
								selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
									attribute, enteredText);
							}
						}
						break;
					case AttributeControlType.Datepicker:
						{
							var day = form[controlId + "_day"];
							var month = form[controlId + "_month"];
							var year = form[controlId + "_year"];
							DateTime? selectedDate = null;
							try
							{
								selectedDate = new DateTime(Int32.Parse(year), Int32.Parse(month), Int32.Parse(day));
							}
							catch { }
							if (selectedDate.HasValue)
							{
								selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
									attribute, selectedDate.Value.ToString("D"));
							}
						}
						break;
					case AttributeControlType.FileUpload:
						{
							//var httpPostedFile = this.Request.Files[controlId];
							//if ((httpPostedFile != null) && (!String.IsNullOrEmpty(httpPostedFile.FileName)))
							//{
							//    int fileMaxSize = _catalogSettings.FileUploadMaximumSizeBytes;
							//    if (httpPostedFile.ContentLength > fileMaxSize)
							//    {
							//        addToCartWarnings.Add(string.Format(_localizationService.GetResource("ShoppingCart.MaximumUploadedFileSize"), (int)(fileMaxSize / 1024)));
							//    }
							//    else
							//    {
							//        //save an uploaded file
							//        var download = new Download()
							//        {
							//            DownloadGuid = Guid.NewGuid(),
							//            UseDownloadUrl = false,
							//            DownloadUrl = "",
							//            DownloadBinary = httpPostedFile.GetDownloadBits(),
							//            ContentType = httpPostedFile.ContentType,
							//            Filename = System.IO.Path.GetFileNameWithoutExtension(httpPostedFile.FileName),
							//            Extension = System.IO.Path.GetExtension(httpPostedFile.FileName),
							//            IsNew = true
							//        };
							//        _downloadService.InsertDownload(download);
							//        //save attribute
							//        selectedAttributes = _productAttributeParser.AddProductAttribute(selectedAttributes,
							//            attribute, download.DownloadGuid.ToString());
							//    }
							//}
						}
						break;
					default:
						break;
				}
			}
			attributes = selectedAttributes;

			#endregion

			#region Gift cards

			string recipientName = "";
			string recipientEmail = "";
			string senderName = "";
			string senderEmail = "";
			string giftCardMessage = "";
			if (productVariant.IsGiftCard)
			{
				foreach (string formKey in form.Keys)
				{
					if (formKey.Equals(string.Format("giftcard_{0}.RecipientName", productVariantId), StringComparison.InvariantCultureIgnoreCase))
					{
						recipientName = form[formKey];
						continue;
					}
					if (formKey.Equals(string.Format("giftcard_{0}.RecipientEmail", productVariantId), StringComparison.InvariantCultureIgnoreCase))
					{
						recipientEmail = form[formKey];
						continue;
					}
					if (formKey.Equals(string.Format("giftcard_{0}.SenderName", productVariantId), StringComparison.InvariantCultureIgnoreCase))
					{
						senderName = form[formKey];
						continue;
					}
					if (formKey.Equals(string.Format("giftcard_{0}.SenderEmail", productVariantId), StringComparison.InvariantCultureIgnoreCase))
					{
						senderEmail = form[formKey];
						continue;
					}
					if (formKey.Equals(string.Format("giftcard_{0}.Message", productVariantId), StringComparison.InvariantCultureIgnoreCase))
					{
						giftCardMessage = form[formKey];
						continue;
					}
				}

				attributes = _productAttributeParser.AddGiftCardAttribute(attributes,
					recipientName, recipientEmail, senderName, senderEmail, giftCardMessage);
			}

			#endregion

			//save item
			//addToCartWarnings.AddRange(_shoppingCartService.AddToCart(_workContext.CurrentCustomer,
			//    productVariant, cartType, attributes, customerEnteredPriceConverted, quantity, true));

			addToCartWarnings.AddRange(_shoppingCartService.AddToCart(_workContext.CurrentCustomer,
				   product, cartType, _storeContext.CurrentStore.Id,
				   attributes, customerEnteredPriceConverted,
				   null, null, quantity, true));

			#region Set already entered values

			//set already entered values (quantity, customer entered price, gift card attributes, product attributes
			//we do it manually because views do not use HTML helpers for rendering controls

			Action<ProductDetailsModel> setEnteredValues = (productModel) =>
			{
				//find product variant model
				var productVariantModel = productModel;
				if (productVariantModel == null)
					return;

				#region 'Add to cart' model

				//entered quantity
				productVariantModel.AddToCart.EnteredQuantity = quantity;
				//allowed quantities
				var allowedQuantities = _productService.ParseAllowedQuantities(productVariant);
				if (allowedQuantities.Length > 0)
				{
					var allowedQuantitySelectedItem = productVariantModel.AddToCart.AllowedQuantities
						.Where(x => x.Text == quantity.ToString())
						.FirstOrDefault();
					if (allowedQuantitySelectedItem != null)
					{
						allowedQuantitySelectedItem.Selected = true;
					}
				}

				//customer entered price
				if (productVariantModel.AddToCart.CustomerEntersPrice)
				{
					productVariantModel.AddToCart.CustomerEnteredPrice = customerEnteredPrice;
				}

				#endregion

				#region Gift card attributes

				if (productVariant.IsGiftCard)
				{
					productVariantModel.GiftCard.RecipientName = recipientName;
					productVariantModel.GiftCard.RecipientEmail = recipientEmail;
					productVariantModel.GiftCard.SenderName = senderName;
					productVariantModel.GiftCard.SenderEmail = senderEmail;
					productVariantModel.GiftCard.Message = giftCardMessage;
				}

				#endregion

				#region Product attributes
				//clear pre-defined values)
				foreach (var pvaModel in productVariantModel.ProductAttributes)
				{
					foreach (var pvavModel in pvaModel.Values)
						pvavModel.IsPreSelected = false;
				}
				//select the previously entered ones
				foreach (var attribute in productVariantAttributes)
				{
					//string controlId = string.Format("product_attribute_{0}_{1}_{2}", attribute.ProductId, attribute.ProductAttributeId, attribute.Id);
					var controlId = $"{NopAttributePrefixDefaults.Product}{attribute.Id}";
					switch (attribute.AttributeControlType)
					{
						case AttributeControlType.DropdownList:
							{
								var ddlAttributes = form[controlId];
								if (!String.IsNullOrEmpty(ddlAttributes))
								{
									int selectedAttributeId = int.Parse(ddlAttributes);
									if (selectedAttributeId > 0)
									{
										var pvavModel = productVariantModel.ProductAttributes
											.SelectMany(x => x.Values)
											.Where(y => y.Id == selectedAttributeId)
											.FirstOrDefault();
										if (pvavModel != null)
											pvavModel.IsPreSelected = true;
									}
								}
							}
							break;
						case AttributeControlType.RadioList:
							{
								var rblAttributes = form[controlId];
								if (!String.IsNullOrEmpty(rblAttributes))
								{
									int selectedAttributeId = int.Parse(rblAttributes);
									if (selectedAttributeId > 0)
									{
										var pvavModel = productVariantModel.ProductAttributes
											.SelectMany(x => x.Values)
											.Where(y => y.Id == selectedAttributeId)
											.FirstOrDefault();
										if (pvavModel != null)
											pvavModel.IsPreSelected = true;
									}
								}
							}
							break;
						case AttributeControlType.Checkboxes:
							{
								var cblAttributes = form[controlId];
								if (!String.IsNullOrEmpty(cblAttributes))
								{
									foreach (var item in cblAttributes.ToString()
									.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
									{
										int selectedAttributeId = int.Parse(item);
										if (selectedAttributeId > 0)
										{
											var pvavModel = productVariantModel.ProductAttributes
											   .SelectMany(x => x.Values)
											   .Where(y => y.Id == selectedAttributeId)
											   .FirstOrDefault();
											if (pvavModel != null)
												pvavModel.IsPreSelected = true;
										}
									}
								}
							}
							break;
						case AttributeControlType.TextBox:
							{
								var txtAttribute = form[controlId];
								if (!String.IsNullOrEmpty(txtAttribute))
								{
									var pvaModel = productVariantModel
										.ProductAttributes
										.Select(x => x)
										.Where(y => y.Id == attribute.Id)
										.FirstOrDefault();

									if (pvaModel != null)
										pvaModel.TextPrompt = txtAttribute;
								}
							}
							break;
						case AttributeControlType.MultilineTextbox:
							{
								var txtAttribute = form[controlId];
								if (!String.IsNullOrEmpty(txtAttribute))
								{
									var pvaModel = productVariantModel
										.ProductAttributes
										.Select(x => x)
										.Where(y => y.Id == attribute.Id)
										.FirstOrDefault();

									if (pvaModel != null)
										pvaModel.TextPrompt = txtAttribute;
								}
							}
							break;
						case AttributeControlType.Datepicker:
							{
								var pvaModel = productVariantModel
									.ProductAttributes
									.Select(x => x)
									.Where(y => y.Id == attribute.Id)
									.FirstOrDefault();
								if (pvaModel != null)
								{
									int day, month, year;
									if (int.TryParse(form[controlId + "_day"], out day))
										pvaModel.SelectedDay = day;
									if (int.TryParse(form[controlId + "_month"], out month))
										pvaModel.SelectedMonth = month;
									if (int.TryParse(form[controlId + "_year"], out year))
										pvaModel.SelectedYear = year;
								}
							}
							break;
						default:
							break;
					}
				}

				#endregion
			};

			#endregion

			#region Return the view

			if (addToCartWarnings.Count == 0)
			{
				switch (cartType)
				{
					case ShoppingCartType.Wishlist:
						{
							if (_shoppingCartSettings.DisplayWishlistAfterAddingProduct)
							{
								//redirect to the wishlist page
								return RedirectToRoute("Wishlist");
							}
							else
							{
								//redisplay the page with "Product has been added to the wishlist" notification message
								var model = _productModelFactory.PrepareProductDetailsModel(product);
								_notificationService.SuccessNotification(_localizationService.GetResource("Products.ProductHasBeenAddedToTheWishlist"), false);
								//set already entered values (quantity, customer entered price, gift card attributes, product attributes)
								setEnteredValues(model);

								//template
								var productTemplateViewPath = _productModelFactory.PrepareProductTemplateViewPath(product);

								return View(productTemplateViewPath, model);
							}
						}
					case ShoppingCartType.ShoppingCart:
					default:
						{

							//if (_shoppingCartSettings.DisplayCartAfterAddingProduct)
							//{
							//    //redirect to the shopping cart page
							//    return RedirectToRoute("ShoppingCart");
							//}
							//else
							//{
							//    //redisplay the page with "Product has been added to the cart" notification message
							//    var model = _productModelFactory.PrepareProductDetailsModel(product);
							//    this.SuccessNotification(_localizationService.GetResource("Products.ProductHasBeenAddedToTheCart"), false);
							//    //set already entered values (quantity, customer entered price, gift card attributes, product attributes)
							//    setEnteredValues(model);
							//    return View(model.ProductTemplateViewPath, model);
							//}

							return RedirectToRoute("ShoppingCart");
						}
				}
			}
			else
			{
				//Errors
				foreach (string error in addToCartWarnings)
					ModelState.AddModelError("", error);

				//If we got this far, something failed, redisplay form
				var model = _productModelFactory.PrepareProductDetailsModel(product);
				//set already entered values (quantity, customer entered price, gift card attributes, product attributes
				setEnteredValues(model);

				//template
				var productTemplateViewPath = _productModelFactory.PrepareProductTemplateViewPath(product);

				return View(productTemplateViewPath, model);
			}

			#endregion
		}

		//end of customization

		#endregion

		#endregion

		#region Recently viewed products

		[HttpsRequirement(SslRequirement.No)]
		public virtual IActionResult RecentlyViewedProducts()
		{
			if (!_catalogSettings.RecentlyViewedProductsEnabled)
				return Content("");

			var products = _recentlyViewedProductsService.GetRecentlyViewedProducts(_catalogSettings.RecentlyViewedProductsNumber);

			var model = new List<ProductOverviewModel>();
			model.AddRange(_productModelFactory.PrepareProductOverviewModels(products));

			return View(model);
		}

		#endregion

		#region New (recently added) products page

		[HttpsRequirement(SslRequirement.No)]
		public virtual IActionResult NewProducts()
		{
			if (!_catalogSettings.NewProductsEnabled)
				return Content("");

			var products = _productService.SearchProducts(
				storeId: _storeContext.CurrentStore.Id,
				visibleIndividuallyOnly: true,
				markedAsNewOnly: true,
				orderBy: ProductSortingEnum.CreatedOn,
				pageSize: _catalogSettings.NewProductsNumber);

			var model = new List<ProductOverviewModel>();
			model.AddRange(_productModelFactory.PrepareProductOverviewModels(products));

			return View(model);
		}

		public virtual IActionResult NewProductsRss()
		{
			var feed = new RssFeed(
				$"{_localizationService.GetLocalized(_storeContext.CurrentStore, x => x.Name)}: New products",
				"Information about products",
				new Uri(_webHelper.GetStoreLocation()),
				DateTime.UtcNow);

			if (!_catalogSettings.NewProductsEnabled)
				return new RssActionResult(feed, _webHelper.GetThisPageUrl(false));

			var items = new List<RssItem>();

			var products = _productService.SearchProducts(
				storeId: _storeContext.CurrentStore.Id,
				visibleIndividuallyOnly: true,
				markedAsNewOnly: true,
				orderBy: ProductSortingEnum.CreatedOn,
				pageSize: _catalogSettings.NewProductsNumber);
			foreach (var product in products)
			{
				var productUrl = Url.RouteUrl("Product", new { SeName = _urlRecordService.GetSeName(product) }, _webHelper.CurrentRequestProtocol);
				var productName = _localizationService.GetLocalized(product, x => x.Name);
				var productDescription = _localizationService.GetLocalized(product, x => x.ShortDescription);
				var item = new RssItem(productName, productDescription, new Uri(productUrl), $"urn:store:{_storeContext.CurrentStore.Id}:newProducts:product:{product.Id}", product.CreatedOnUtc);
				items.Add(item);
				//uncomment below if you want to add RSS enclosure for pictures
				//var picture = _pictureService.GetPicturesByProductId(product.Id, 1).FirstOrDefault();
				//if (picture != null)
				//{
				//    var imageUrl = _pictureService.GetPictureUrl(picture, _mediaSettings.ProductDetailsPictureSize);
				//    item.ElementExtensions.Add(new XElement("enclosure", new XAttribute("type", "image/jpeg"), new XAttribute("url", imageUrl), new XAttribute("length", picture.PictureBinary.Length)));
				//}

			}
			feed.Items = items;
			return new RssActionResult(feed, _webHelper.GetThisPageUrl(false));
		}

		#endregion

		#region Product reviews

		[HttpsRequirement(SslRequirement.No)]
		public virtual IActionResult ProductReviews(int productId)
		{
			var product = _productService.GetProductById(productId);
			if (product == null || product.Deleted || !product.Published || !product.AllowCustomerReviews)
				return RedirectToRoute("Homepage");

			var model = new ProductReviewsModel();
			model = _productModelFactory.PrepareProductReviewsModel(model, product);
			//only registered users can leave reviews
			if (_workContext.CurrentCustomer.IsGuest() && !_catalogSettings.AllowAnonymousUsersToReviewProduct)
				ModelState.AddModelError("", _localizationService.GetResource("Reviews.OnlyRegisteredUsersCanWriteReviews"));

			if (_catalogSettings.ProductReviewPossibleOnlyAfterPurchasing)
			{
				var hasCompletedOrders = _orderService.SearchOrders(customerId: _workContext.CurrentCustomer.Id,
					productId: productId,
					osIds: new List<int> { (int)OrderStatus.Complete },
					pageSize: 1).Any();
				if (!hasCompletedOrders)
					ModelState.AddModelError(string.Empty, _localizationService.GetResource("Reviews.ProductReviewPossibleOnlyAfterPurchasing"));
			}

			//default value
			model.AddProductReview.Rating = _catalogSettings.DefaultProductRatingValue;

			//default value for all additional review types
			if (model.ReviewTypeList.Count > 0)
				foreach (var additionalProductReview in model.AddAdditionalProductReviewList)
				{
					additionalProductReview.Rating = additionalProductReview.IsRequired ? _catalogSettings.DefaultProductRatingValue : 0;
				}

			return View(model);
		}

		[HttpPost, ActionName("ProductReviews")]
		[PublicAntiForgery]
		[FormValueRequired("add-review")]
		[ValidateCaptcha]
		public virtual IActionResult ProductReviewsAdd(int productId, ProductReviewsModel model, bool captchaValid)
		{
			var product = _productService.GetProductById(productId);
			if (product == null || product.Deleted || !product.Published || !product.AllowCustomerReviews)
				return RedirectToRoute("Homepage");

			//validate CAPTCHA
			if (_captchaSettings.Enabled && _captchaSettings.ShowOnProductReviewPage && !captchaValid)
			{
				ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptchaMessage"));
			}

			if (_workContext.CurrentCustomer.IsGuest() && !_catalogSettings.AllowAnonymousUsersToReviewProduct)
			{
				ModelState.AddModelError("", _localizationService.GetResource("Reviews.OnlyRegisteredUsersCanWriteReviews"));
			}

			if (_catalogSettings.ProductReviewPossibleOnlyAfterPurchasing)
			{
				var hasCompletedOrders = _orderService.SearchOrders(customerId: _workContext.CurrentCustomer.Id,
					productId: productId,
					osIds: new List<int> { (int)OrderStatus.Complete },
					pageSize: 1).Any();
				if (!hasCompletedOrders)
					ModelState.AddModelError(string.Empty, _localizationService.GetResource("Reviews.ProductReviewPossibleOnlyAfterPurchasing"));
			}

			if (ModelState.IsValid)
			{
				//save review
				var rating = model.AddProductReview.Rating;
				if (rating < 1 || rating > 5)
					rating = _catalogSettings.DefaultProductRatingValue;
				var isApproved = !_catalogSettings.ProductReviewsMustBeApproved;

				var productReview = new ProductReview
				{
					ProductId = product.Id,
					CustomerId = _workContext.CurrentCustomer.Id,
					Title = model.AddProductReview.Title,
					ReviewText = model.AddProductReview.ReviewText,
					Rating = rating,
					HelpfulYesTotal = 0,
					HelpfulNoTotal = 0,
					IsApproved = isApproved,
					CreatedOnUtc = DateTime.UtcNow,
					StoreId = _storeContext.CurrentStore.Id,
				};

				product.ProductReviews.Add(productReview);

				//add product review and review type mapping                
				foreach (var additionalReview in model.AddAdditionalProductReviewList)
				{
					var additionalProductReview = new ProductReviewReviewTypeMapping
					{
						ProductReviewId = productReview.Id,
						ReviewTypeId = additionalReview.ReviewTypeId,
						Rating = additionalReview.Rating
					};
					productReview.ProductReviewReviewTypeMappingEntries.Add(additionalProductReview);
				}

				//update product totals
				_productService.UpdateProductReviewTotals(product);

				//notify store owner
				if (_catalogSettings.NotifyStoreOwnerAboutNewProductReviews)
					_workflowMessageService.SendProductReviewNotificationMessage(productReview, _localizationSettings.DefaultAdminLanguageId);

				//activity log
				_customerActivityService.InsertActivity("PublicStore.AddProductReview",
					string.Format(_localizationService.GetResource("ActivityLog.PublicStore.AddProductReview"), product.Name), product);

				//raise event
				if (productReview.IsApproved)
					_eventPublisher.Publish(new ProductReviewApprovedEvent(productReview));

				model = _productModelFactory.PrepareProductReviewsModel(model, product);
				model.AddProductReview.Title = null;
				model.AddProductReview.ReviewText = null;

				model.AddProductReview.SuccessfullyAdded = true;
				if (!isApproved)
					model.AddProductReview.Result = _localizationService.GetResource("Reviews.SeeAfterApproving");
				else
					model.AddProductReview.Result = _localizationService.GetResource("Reviews.SuccessfullyAdded");

				return View(model);
			}

			//If we got this far, something failed, redisplay form
			model = _productModelFactory.PrepareProductReviewsModel(model, product);
			return View(model);
		}

		[HttpPost]
		public virtual IActionResult SetProductReviewHelpfulness(int productReviewId, bool washelpful)
		{
			var productReview = _productService.GetProductReviewById(productReviewId);
			if (productReview == null)
				throw new ArgumentException("No product review found with the specified id");

			if (_workContext.CurrentCustomer.IsGuest() && !_catalogSettings.AllowAnonymousUsersToReviewProduct)
			{
				return Json(new
				{
					Result = _localizationService.GetResource("Reviews.Helpfulness.OnlyRegistered"),
					TotalYes = productReview.HelpfulYesTotal,
					TotalNo = productReview.HelpfulNoTotal
				});
			}

			//customers aren't allowed to vote for their own reviews
			if (productReview.CustomerId == _workContext.CurrentCustomer.Id)
			{
				return Json(new
				{
					Result = _localizationService.GetResource("Reviews.Helpfulness.YourOwnReview"),
					TotalYes = productReview.HelpfulYesTotal,
					TotalNo = productReview.HelpfulNoTotal
				});
			}

			//delete previous helpfulness
			var prh = productReview.ProductReviewHelpfulnessEntries
				.FirstOrDefault(x => x.CustomerId == _workContext.CurrentCustomer.Id);
			if (prh != null)
			{
				//existing one
				prh.WasHelpful = washelpful;
			}
			else
			{
				//insert new helpfulness
				prh = new ProductReviewHelpfulness
				{
					ProductReviewId = productReview.Id,
					CustomerId = _workContext.CurrentCustomer.Id,
					WasHelpful = washelpful,
				};
				productReview.ProductReviewHelpfulnessEntries.Add(prh);
			}
			_productService.UpdateProduct(productReview.Product);

			//new totals
			productReview.HelpfulYesTotal = productReview.ProductReviewHelpfulnessEntries.Count(x => x.WasHelpful);
			productReview.HelpfulNoTotal = productReview.ProductReviewHelpfulnessEntries.Count(x => !x.WasHelpful);
			_productService.UpdateProduct(productReview.Product);

			return Json(new
			{
				Result = _localizationService.GetResource("Reviews.Helpfulness.SuccessfullyVoted"),
				TotalYes = productReview.HelpfulYesTotal,
				TotalNo = productReview.HelpfulNoTotal
			});
		}

		public virtual IActionResult CustomerProductReviews(int? pageNumber)
		{
			if (_workContext.CurrentCustomer.IsGuest())
				return Challenge();

			if (!_catalogSettings.ShowProductReviewsTabOnAccountPage)
			{
				return RedirectToRoute("CustomerInfo");
			}

			var model = _productModelFactory.PrepareCustomerProductReviewsModel(pageNumber);
			return View(model);
		}

		#endregion

		#region Email a friend

		[HttpsRequirement(SslRequirement.No)]
		public virtual IActionResult ProductEmailAFriend(int productId)
		{
			var product = _productService.GetProductById(productId);
			if (product == null || product.Deleted || !product.Published || !_catalogSettings.EmailAFriendEnabled)
				return RedirectToRoute("Homepage");

			var model = new ProductEmailAFriendModel();
			model = _productModelFactory.PrepareProductEmailAFriendModel(model, product, false);
			return View(model);
		}

		[HttpPost, ActionName("ProductEmailAFriend")]
		[PublicAntiForgery]
		[FormValueRequired("send-email")]
		[ValidateCaptcha]
		public virtual IActionResult ProductEmailAFriendSend(ProductEmailAFriendModel model, bool captchaValid)
		{
			var product = _productService.GetProductById(model.ProductId);
			if (product == null || product.Deleted || !product.Published || !_catalogSettings.EmailAFriendEnabled)
				return RedirectToRoute("Homepage");

			//validate CAPTCHA
			if (_captchaSettings.Enabled && _captchaSettings.ShowOnEmailProductToFriendPage && !captchaValid)
			{
				ModelState.AddModelError("", _localizationService.GetResource("Common.WrongCaptchaMessage"));
			}

			//check whether the current customer is guest and ia allowed to email a friend
			if (_workContext.CurrentCustomer.IsGuest() && !_catalogSettings.AllowAnonymousUsersToEmailAFriend)
			{
				ModelState.AddModelError("", _localizationService.GetResource("Products.EmailAFriend.OnlyRegisteredUsers"));
			}

			if (ModelState.IsValid)
			{
				//email
				_workflowMessageService.SendProductEmailAFriendMessage(_workContext.CurrentCustomer,
						_workContext.WorkingLanguage.Id, product,
						model.YourEmailAddress, model.FriendEmail,
						Core.Html.HtmlHelper.FormatText(model.PersonalMessage, false, true, false, false, false, false));

				model = _productModelFactory.PrepareProductEmailAFriendModel(model, product, true);
				model.SuccessfullySent = true;
				model.Result = _localizationService.GetResource("Products.EmailAFriend.SuccessfullySent");

				return View(model);
			}

			//If we got this far, something failed, redisplay form
			model = _productModelFactory.PrepareProductEmailAFriendModel(model, product, true);
			return View(model);
		}

		#endregion

		#region Comparing products

		[HttpPost]
		public virtual IActionResult AddProductToCompareList(int productId)
		{
			var product = _productService.GetProductById(productId);
			if (product == null || product.Deleted || !product.Published)
				return Json(new
				{
					success = false,
					message = "No product found with the specified ID"
				});

			if (!_catalogSettings.CompareProductsEnabled)
				return Json(new
				{
					success = false,
					message = "Product comparison is disabled"
				});

			_compareProductsService.AddProductToCompareList(productId);

			//activity log
			_customerActivityService.InsertActivity("PublicStore.AddToCompareList",
				string.Format(_localizationService.GetResource("ActivityLog.PublicStore.AddToCompareList"), product.Name), product);

			return Json(new
			{
				success = true,
				message = string.Format(_localizationService.GetResource("Products.ProductHasBeenAddedToCompareList.Link"), Url.RouteUrl("CompareProducts"))
				//use the code below (commented) if you want a customer to be automatically redirected to the compare products page
				//redirect = Url.RouteUrl("CompareProducts"),
			});
		}

		public virtual IActionResult RemoveProductFromCompareList(int productId)
		{
			var product = _productService.GetProductById(productId);
			if (product == null)
				return RedirectToRoute("Homepage");

			if (!_catalogSettings.CompareProductsEnabled)
				return RedirectToRoute("Homepage");

			_compareProductsService.RemoveProductFromCompareList(productId);

			return RedirectToRoute("CompareProducts");
		}

		[HttpsRequirement(SslRequirement.No)]
		public virtual IActionResult CompareProducts()
		{
			if (!_catalogSettings.CompareProductsEnabled)
				return RedirectToRoute("Homepage");

			var model = new CompareProductsModel
			{
				IncludeShortDescriptionInCompareProducts = _catalogSettings.IncludeShortDescriptionInCompareProducts,
				IncludeFullDescriptionInCompareProducts = _catalogSettings.IncludeFullDescriptionInCompareProducts,
			};

			var products = _compareProductsService.GetComparedProducts();

			//ACL and store mapping
			products = products.Where(p => _aclService.Authorize(p) && _storeMappingService.Authorize(p)).ToList();
			//availability dates
			products = products.Where(p => _productService.ProductIsAvailable(p)).ToList();

			//prepare model
			_productModelFactory.PrepareProductOverviewModels(products, prepareSpecificationAttributes: true)
				.ToList()
				.ForEach(model.Products.Add);
			return View(model);
		}

		public virtual IActionResult ClearCompareList()
		{
			if (!_catalogSettings.CompareProductsEnabled)
				return RedirectToRoute("Homepage");

			_compareProductsService.ClearCompareProducts();

			return RedirectToRoute("CompareProducts");
		}

		#endregion

		#region Custom

		#region Custom Utilities

		protected virtual IList<string> PrepareColoursImageUrl(OrderItem orderItem)
		{
			#region Custom

			try
			{
				#region Pick Image For Cart

				var pickColors = _productAttributeFormatter.FormatAttributesForTitle(orderItem.Product, orderItem.AttributesXml);

				Regex regexSpring = new Regex(@"\bSpring Set: \b");
				Regex regexFrontLeft = new Regex(@"\b<br />Front Left: \b");
				Regex regexFrontRight = new Regex(@"\b<br />Front Right: \b");
				Regex regexBackLeft = new Regex(@"\b<br />Back Left: \b");
				Regex regexBackRight = new Regex(@"\b<br />Back Right: \b");

				#region For Spring

				string[] substringsSpring = regexSpring.Split(pickColors);
				regexSpring = new Regex(@"\b<br />Front Left: \b");
				substringsSpring = regexFrontLeft.Split(substringsSpring[1]);

				#endregion

				#region Only For FrontLeft

				string[] substringsFrontLeft = regexFrontLeft.Split(pickColors);
				regexFrontLeft = new Regex(@"\b<br />Front Right: \b");
				substringsFrontLeft = regexFrontLeft.Split(substringsFrontLeft[1]);

				#endregion

				#region Only For FrontRight

				string[] substringsFrontRight = regexFrontRight.Split(substringsFrontLeft[1]);//regexFrontRight.Split(pickColors);
				regexFrontRight = new Regex(@"\b<br />Back Left: \b");
				substringsFrontRight = regexFrontRight.Split(substringsFrontRight[0]);

				#endregion

				#region Only For BackLeft

				string[] substringsBackLeft = regexBackLeft.Split(substringsFrontRight[1]);//regexBackLeft.Split(pickColors);
				regexBackLeft = new Regex(@"\b<br />Back Right: \b");
				substringsBackLeft = regexBackLeft.Split(substringsBackLeft[0]);

				#endregion


				#region Only For BackRight

				string[] substringsBackRight = regexBackRight.Split(substringsBackLeft[1]);//regexBackRight.Split(pickColors);
				regexBackRight = new Regex(@"\b<br />Blade Make: \b");
				substringsBackRight = regexBackRight.Split(substringsBackRight[0]);

				#endregion


				int cnt = 0;
				var spring = string.Empty;
				var frontLeftColor = string.Empty;
				var frontRightColor = string.Empty;
				var backLeftColor = string.Empty;
				var backRightColor = string.Empty;

				foreach (string match in substringsSpring)
				{
					if (cnt == 0)
					{
						//pickCol.spring = match + ".png";
						spring = "/Images/design/" + "Spring_" + match + ".png";
					}
					cnt += 1;
				}

				cnt = 0;
				foreach (string match in substringsFrontLeft)
				{
					if (cnt == 0)
					{
						//pickCol.frontLeftColor = match + ".png";
						frontLeftColor = "/Images/design/" + "Left_" + match + ".png";
					}
					cnt += 1;
				}
				cnt = 0;
				foreach (string match in substringsFrontRight)
				{
					if (cnt == 0)
					{
						//pickCol.frontRightColor = match + ".png";
						frontRightColor = "/Images/design/" + "Right_" + match + ".png";
					}
					cnt += 1;
				}
				cnt = 0;
				foreach (string match in substringsBackLeft)
				{
					if (cnt == 0)
					{
						//pickCol.backLeftColor = match + ".png";
						backLeftColor = "/Images/design/" + "Left_" + match + ".png";
					}
					cnt += 1;
				}
				cnt = 0;
				foreach (string match in substringsBackRight)
				{
					if (cnt == 0)
					{
						//pickCol.backRightColor = match + ".png";
						backRightColor = "/Images/design/" + "Right_" + match + ".png";
					}
					cnt += 1;
				}

				List<string> colors = new List<string>();
				colors.Add(frontLeftColor);
				colors.Add(frontRightColor);
				colors.Add(spring);
				colors.Add(backLeftColor);
				colors.Add(backRightColor);

				//Adding colors in list
				return colors;

				#endregion
				//end
			}
			catch (Exception)
			{
				//ignore for now
				return new List<string>();
			}

			#endregion
		}

		#endregion

		#region Order/Product Review

		//Ajax Call
		public virtual string AddProductReviewAjax(int orderId)
		{
			var order = _orderService.GetOrderById(orderId);
			if (order == null)
				return "Order not found!";

			var orderItem = order.OrderItems.FirstOrDefault();
			var product = orderItem.Product;
			if (product == null || product.Deleted || !product.Published)
				return "";

			var model = new ProductReviewsModel();
			model = _productModelFactory.PrepareProductReviewsModel(model, product);

			//fill custom model values
			model.OrderId = order.Id;
			var pictureId = product.ProductPictures.OrderByDescending(x => x.PictureId).FirstOrDefault(x => x.PictureId > 0)?.PictureId ?? 0;
			var picture = _pictureService.GetPictureById(pictureId);
			model.ProductPictureUrl = picture != null ? _pictureService.GetPictureUrl(picture) : string.Empty;
			model.ProductShortDescription = product.ShortDescription;

			//product price,
			if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
			{
				//including tax
				var unitPriceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceInclTax, order.CurrencyRate);
				model.ProductPrice = _priceFormatter.FormatPrice(unitPriceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
			}
			else
			{
				//excluding tax
				var unitPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceExclTax, order.CurrencyRate);
				model.ProductPrice = _priceFormatter.FormatPrice(unitPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
			}
			//colour images
			model.ListColors = PrepareColoursImageUrl(orderItem);

			var viewPath = "~/Themes/" + _themeContext.WorkingThemeName.ToString() + "/Views/Order/OrderReview.cshtml";
			return RenderPartialViewToString(viewPath, model);
		}

		[HttpPost]
		public virtual IActionResult SaveProductReviewAjax(ProductReviewsModel model, IFormCollection form)
		{
			try
			{
				//product
				var product = _productService.GetProductById(model.ProductId);
				if (product == null || product.Deleted || !product.Published)
					return Json(new
					{
						error = 1,
						success = false,
						message = "Product not found!"
					});

				//order
				var order = _orderService.GetOrderById(model.OrderId);
				if (order == null)
					return Json(new
					{
						error = 1,
						success = false,
						message = "Order not found! Id: " + model.OrderId
					});

				//check review already submitted
				var isOrderReviewAlreadySubmitted = _genericAttributeService.GetAttribute<bool>(order, NopCustomerDefaults.IsOrderReviewSubmittedAttribute, defaultValue: false);
				if (isOrderReviewAlreadySubmitted)
					return Json(new
					{
						error = 1,
						success = false,
						message = "Order review already submitted! Id: " + model.OrderId
					});

				if (ModelState.IsValid)
				{
					//save review
					var rating = model.AddProductReview.Rating;
					if (rating < 1 || rating > 5)
						rating = _catalogSettings.DefaultProductRatingValue;
					var isApproved = !_catalogSettings.ProductReviewsMustBeApproved;

					var productReview = new ProductReview
					{
						ProductId = product.Id,
						CustomerId = _workContext.CurrentCustomer.Id,
						Title = model.AddProductReview.Title,
						ReviewText = model.AddProductReview.ReviewText + string.Format(" For Order #{0}", model.OrderId),
						Rating = rating,
						HelpfulYesTotal = 0,
						HelpfulNoTotal = 0,
						IsApproved = isApproved,
						CreatedOnUtc = DateTime.UtcNow,
						StoreId = _storeContext.CurrentStore.Id,
					};

					product.ProductReviews.Add(productReview);

					//add product review and review type mapping                
					foreach (var additionalReview in model.AddAdditionalProductReviewList)
					{
						var additionalProductReview = new ProductReviewReviewTypeMapping
						{
							ProductReviewId = productReview.Id,
							ReviewTypeId = additionalReview.ReviewTypeId,
							Rating = additionalReview.Rating
						};
						productReview.ProductReviewReviewTypeMappingEntries.Add(additionalProductReview);
					}

					//update product totals
					_productService.UpdateProductReviewTotals(product);

					//notify store owner
					if (_catalogSettings.NotifyStoreOwnerAboutNewProductReviews)
						_workflowMessageService.SendProductReviewNotificationMessage(productReview, _localizationSettings.DefaultAdminLanguageId);

					//activity log
					_customerActivityService.InsertActivity("PublicStore.AddProductReview",
						string.Format(_localizationService.GetResource("ActivityLog.PublicStore.AddProductReview"), product.Name), product);

					//raise event
					if (productReview.IsApproved)
						_eventPublisher.Publish(new ProductReviewApprovedEvent(productReview));

					//add orderReview mapping entry in generic attributes
					_genericAttributeService.SaveAttribute(order, NopCustomerDefaults.IsOrderReviewSubmittedAttribute, true);

					return Json(new
					{
						success = true,
						orderId = model.OrderId,
						productId = model.ProductId,
						message = _localizationService.GetResource("Nb.Order.Review.Submit.Success.Text")
					});
				}

				//If we got this far, something failed, redisplay form
				model = _productModelFactory.PrepareProductReviewsModel(model, product);

				//fill custom model values
				var orderItem = order.OrderItems.FirstOrDefault();
				model.OrderId = order.Id;
				var pictureId = product.ProductPictures.OrderByDescending(x => x.PictureId).FirstOrDefault(x => x.PictureId > 0)?.PictureId ?? 0;
				var picture = _pictureService.GetPictureById(pictureId);
				model.ProductPictureUrl = picture != null ? _pictureService.GetPictureUrl(picture) : string.Empty;
				model.ProductShortDescription = product.ShortDescription;

				//product price,
				if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
				{
					//including tax
					var unitPriceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceInclTax, order.CurrencyRate);
					model.ProductPrice = _priceFormatter.FormatPrice(unitPriceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
				}
				else
				{
					//excluding tax
					var unitPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceExclTax, order.CurrencyRate);
					model.ProductPrice = _priceFormatter.FormatPrice(unitPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
				}
				//colour images
				model.ListColors = PrepareColoursImageUrl(orderItem);

				//get view
				var viewPath = "~/Themes/" + _themeContext.WorkingThemeName.ToString() + "/Views/Order/OrderReview.cshtml";

				return Json(new
				{
					success = false,
					update_section = new UpdateSectionJsonModel
					{
						name = "dv-review-container",
						html = RenderPartialViewToString(viewPath, model)
					}
				});
			}
			catch (Exception exc)
			{
				return Json(new
				{
					error = 1,
					success = false,
					message = exc.Message
				});
			}
		}

		#endregion

		#endregion
	}
}