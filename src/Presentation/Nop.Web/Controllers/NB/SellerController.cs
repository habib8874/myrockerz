﻿using Microsoft.AspNetCore.Mvc; 

namespace Nop.Web.Controllers.NB
{
    public partial class SellerController : Controller
    {
        #region Fields

        #endregion

        #region Ctor
        public SellerController()
        {

        }

        #endregion

        #region Method

        // Get : Seller Page
        public virtual ActionResult Index()
        {
            return View();
        }

        #endregion
    }
}