﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Nop.Core.Domain.NB;

namespace Nop.Data.Mapping.NB
{
    /// <summary>
    /// Represents a entity mapping configuration
    /// </summary>
    public partial class Sinek_CutPointMap_View : NopEntityTypeConfiguration<Sinek_CutPoint_View>
    {
        #region Methods

        /// <summary>
        /// Configures the entity
        /// </summary>
        /// <param name="builder">The builder to be used to configure the entity</param>
        public override void Configure(EntityTypeBuilder<Sinek_CutPoint_View> builder)
        {
            builder.ToTable("vw_Sinek_CutPoint");
            builder.HasKey(m => m.Id);

            builder.Property(m => m.ManufacturerId);
            builder.Property(m => m.ModelId);
            builder.Property(m => m.Size).IsRequired().HasMaxLength(100);
            builder.Property(m => m.CutPoint).IsRequired().HasMaxLength(100);
            builder.Property(m => m.AdditionalInfo).HasMaxLength(400);
            builder.Property(m => m.PageSize);
            builder.Property(m => m.PageSizeOptions).HasMaxLength(200);
            builder.Property(m => m.Published);
            builder.Property(m => m.Deleted);
            builder.Property(m => m.CreatedOnUtc);
            builder.Property(m => m.UpdatedOnUtc);
            builder.Property(m => m.ManufacturerName);
            builder.Property(m => m.Sinek_ModelName);
            builder.Property(m => m.DisplayOrder);

            base.Configure(builder);
        }

        #endregion
    }
}
