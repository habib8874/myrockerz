﻿using Nop.Core;
using Nop.Core.Domain.NB;
using System.Collections.Generic;

namespace Nop.Services.NB
{
	/// <summary>
	/// Sinek_Model service
	/// </summary>
	public partial interface ISinek_ModelService
    {
        /// <summary>
        /// Deletes a sinek_Model
        /// </summary>
        /// <param name="sinek_Model">Sinek_Model</param>
        void DeleteSinek_Model(Sinek_Model sinek_Model);

        IList<Sinek_Model> GetSinek_ModelByManufacturerId(int manufacturerId, bool showHidden = false);

        /// <summary>
        /// Gets all sinek_Models
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_Model collection</returns>
        IList<Sinek_Model> GetAllSinek_Models(bool showHidden = false);

        /// <summary>
        /// Gets all sinek_Models
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_Model collection</returns>
        IList<Sinek_Model_View> GetAllSinek_Models_View(bool showHidden = false);

        /// <summary>
        /// Gets all sinek_Models
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_Models</returns>
        IPagedList<Sinek_Model_View> GetAllSinek_Models_View(string modelName, int manufactureId, int pageIndex, int pageSize, bool showHidden = false);


        /// <summary>
        /// Gets all sinek_Models
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_Models</returns>
        IPagedList<Sinek_Model> GetAllSinek_Models(int pageIndex, int pageSize, bool showHidden = false);

        /// <summary>
        /// Gets a sinek_Model
        /// </summary>
        /// <param name="sinek_ModelId">Sinek_Model identifier</param>
        /// <returns>Sinek_Model</returns>
        Sinek_Model GetSinek_ModelById(int sinek_ModelId);

        /// <summary>
        /// Inserts a sinek_Model
        /// </summary>
        /// <param name="sinek_Model">Sinek_Model</param>
        void InsertSinek_Model(Sinek_Model sinek_Model);

        /// <summary>
        /// Updates the sinek_Model
        /// </summary>
        /// <param name="sinek_Model">Sinek_Model</param>
        void UpdateSinek_Model(Sinek_Model sinek_Model);

    }
}
