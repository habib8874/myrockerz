﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.NB;

namespace Nop.Services.NB
{
	/// <summary>
	/// Sinek_CutPoint service
	/// </summary>
	public partial interface ISinek_CutPointService
    {
        /// <summary>
        /// Deletes a sinek_CutPoint
        /// </summary>
        /// <param name="sinek_CutPoint">Sinek_CutPoint</param>
        void DeleteSinek_CutPoint(Sinek_CutPoint sinek_CutPoint);

        IList<Sinek_CutPoint> GetSinek_CutPointByManufacturerIdModelId(int modelId, int manufacturerId, bool showHidden = false);

        IList<Sinek_CutPoint> GetSinek_CutPointByModelIId(int modelId, int manufacturerId, bool showHidden = false);

        IList<Sinek_CutPoint> GetSinek_CutPointByManufacturerIdModelIdSize(int modelId, int manufacturerId, string size, bool showHidden = false);

        /// <summary>
        /// Gets all sinek_CutPoints
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_CutPoint collection</returns>
        IList<Sinek_CutPoint> GetAllSinek_CutPoints(bool showHidden = false);

        /// <summary>
        /// Gets all sinek_CutPoints
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_CutPoints</returns>
        IPagedList<Sinek_CutPoint> GetAllSinek_CutPoints(int pageIndex, int pageSize, bool showHidden = false);

        /// <summary>
        /// Gets all sinek_CutPoints
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_CutPoint collection</returns>
        IList<Sinek_CutPoint_View> GetAllSinek_CutPoints_View(bool showHidden = false);

        /// <summary>
        /// Gets all sinek_CutPoints
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_CutPoints</returns>
        IPagedList<Sinek_CutPoint_View> GetAllSinek_CutPoints_View(string modelName, int manufactureId, int pageIndex, int pageSize, bool showHidden = false);


        /// <summary>
        /// Gets a sinek_CutPoint
        /// </summary>
        /// <param name="sinek_CutPointId">Sinek_CutPoint identifier</param>
        /// <returns>Sinek_CutPoint</returns>
        Sinek_CutPoint GetSinek_CutPointById(int sinek_CutPointId);

        /// <summary>
        /// Inserts a sinek_CutPoint
        /// </summary>
        /// <param name="sinek_CutPoint">Sinek_CutPoint</param>
        void InsertSinek_CutPoint(Sinek_CutPoint sinek_CutPoint);

        /// <summary>
        /// Updates the sinek_CutPoint
        /// </summary>
        /// <param name="sinek_CutPoint">Sinek_CutPoint</param>
        void UpdateSinek_CutPoint(Sinek_CutPoint sinek_CutPoint);

    }
}
