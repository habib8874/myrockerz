﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Services.Events;
using Nop.Core.Domain.NB;

namespace Nop.Services.NB
{
	/// <summary>
	/// Sinek_CutPoint service 
	/// </summary>
	public partial class Sinek_CutPointService : ISinek_CutPointService
    {
        #region Constants
        private const string SINEK_CUTPOINT_ALL_KEY = "Nop.sinek_CutPoint.all-{0}";
        private const string SINEK_CUTPOINT_BY_ID_KEY = "Nop.sinek_CutPoint.id-{0}";
        private const string SINEK_CUTPOINT_PATTERN_KEY = "Nop.sinek_CutPoint.";
        public static int ProductVariantAttributeIdForSinek_Cut = 21;
        #endregion

        #region Fields
        private readonly IRepository<Sinek_CutPoint> _sinek_CutPointRepository;
        private readonly IRepository<Sinek_CutPoint_View> _sinek_CutPoint_ViewRepository;
        private readonly IRepository<ProductAttributeValue> _productAttributeValueRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="sinek_CutPointRepository">Category repository</param>
        /// <param name="productSinek_CutPointRepository">ProductCategory repository</param>
        /// <param name="productRepository">Product repository</param>
        /// <param name="eventPublisher">Event published</param>
        public Sinek_CutPointService(ICacheManager cacheManager,
            IRepository<Sinek_CutPoint> sinek_CutPointRepository,
            IRepository<Sinek_CutPoint_View> sinek_CutPoint_ViewRepository,
             IRepository<ProductAttributeValue> productAttributeValueRepository,
            IEventPublisher eventPublisher)
        {
            this._cacheManager = cacheManager;
            this._sinek_CutPointRepository = sinek_CutPointRepository;
            this._sinek_CutPoint_ViewRepository = sinek_CutPoint_ViewRepository;
            this._productAttributeValueRepository = productAttributeValueRepository;
            this._eventPublisher = eventPublisher;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Deletes a sinek_CutPoint
        /// </summary>
        /// <param name="sinek_CutPoint">Sinek_CutPoint</param>
        public virtual void DeleteSinek_CutPoint(Sinek_CutPoint sinek_CutPoint)
        {
            if (sinek_CutPoint == null)
                throw new ArgumentNullException("sinek_CutPoint");

            sinek_CutPoint.Deleted = true;
            UpdateSinek_CutPoint(sinek_CutPoint);
        }

        public virtual void DeleteSinek_CutPoint_ProductVariantAttributeValue(Sinek_CutPoint sinek_CutPoint)
        {
            if (sinek_CutPoint == null)
                throw new ArgumentNullException("sinek_CutPoint");

            #region Here we handle ProductVariantAttributeValue
            //delete Sinek_Model
            ProductAttributeValue objProductVariantAttributeValue = _productAttributeValueRepository.Table.Where(x => x.ProductAttributeMappingId == ProductVariantAttributeIdForSinek_Cut && x.Sinek_CutPointId == sinek_CutPoint.Id).FirstOrDefault();
            if (objProductVariantAttributeValue != null)
            {
                _productAttributeValueRepository.Delete(objProductVariantAttributeValue);
                _eventPublisher.EntityDeleted(objProductVariantAttributeValue);
            }

            //Core.Data.DataSettingsManager dataSettingsManager = new DataSettingsManager();
            //string conString = dataSettingsManager.LoadSettings().DataConnectionString;
            //SqlConnection scCon = new SqlConnection();
            //scCon.ConnectionString = conString;
            //scCon.Open();

            //string sqlCommand = "DELETE FROM ProductVariantAttributeValue " +
            //    "WHERE ProductVariantAttributeId in (" + ProductVariantAttributeIdForSinek_Cut.ToString() + ") AND Sinek_CutPointId = " + sinek_CutPoint.Id.ToString() + " ";
            ////"WHERE ProductVariantAttributeId in (21,22) AND Sinek_CutPointId = " + sinek_CutPoint.Id.ToString() + " ";

            //SqlCommand scCom = new SqlCommand(sqlCommand, scCon);
            //scCom.ExecuteNonQuery();
            //scCon.Close();

            #endregion
        }

        /// <summary>
        /// Gets all sinek_CutPoints
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_CutPoint collection</returns>
        public virtual IList<Sinek_CutPoint> GetAllSinek_CutPoints(bool showHidden = false)
        {
            string key = string.Format(SINEK_CUTPOINT_ALL_KEY, showHidden);
            return _cacheManager.Get(key, () =>
            {
                var query = from m in _sinek_CutPointRepository.Table
                            orderby m.ManufacturerId
                            where (showHidden || m.Published) &&
                            !m.Deleted
                            select m;
                var sinek_CutPoints = query.ToList();
                return sinek_CutPoints;
            });
        }

        /// <summary>
        /// Gets all sinek_CutPoints
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_CutPoints</returns>
        public virtual IPagedList<Sinek_CutPoint> GetAllSinek_CutPoints(int pageIndex, int pageSize, bool showHidden = false)
        {
            var sinek_CutPoints = GetAllSinek_CutPoints(showHidden);
            return new PagedList<Sinek_CutPoint>(sinek_CutPoints, pageIndex, pageSize);
        }

        /// <summary>
        /// Gets all sinek_CutPoints
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_CutPoint collection</returns>
        public virtual IList<Sinek_CutPoint_View> GetAllSinek_CutPoints_View(bool showHidden = false)
        {
            string key = string.Format(SINEK_CUTPOINT_ALL_KEY, showHidden);
            return _cacheManager.Get(key, () =>
            {
                var query = from m in _sinek_CutPoint_ViewRepository.Table
                            orderby m.ManufacturerId
                            where (showHidden || m.Published) &&
                            !m.Deleted
                            select m;
                var sinek_CutPoint_Views = query.ToList();
                return sinek_CutPoint_Views;
            });
        }

        /// <summary>
        /// Gets all sinek_CutPoints
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_CutPoints</returns>
        public virtual IPagedList<Sinek_CutPoint_View> GetAllSinek_CutPoints_View(string modelName, int manufactureId, int pageIndex, int pageSize, bool showHidden = false)
        {
            var query = _sinek_CutPoint_ViewRepository.Table;
            if (!showHidden)
                query = query.Where(m => m.Published);
            if (!string.IsNullOrWhiteSpace(modelName))
                query = query.Where(m => m.Sinek_ModelName.Contains(modelName));
            if (manufactureId > 0)
                query = query.Where(m => m.ManufacturerId == manufactureId);
            query = query.Where(m => !m.Deleted);
            query = query.OrderBy(m => m.DisplayOrder).ThenBy(c => c.Id);

            return new PagedList<Sinek_CutPoint_View>(query.ToList(), pageIndex, pageSize);
        }

        /// <summary>
        /// Gets a sinek_CutPoint
        /// </summary>
        /// <param name="sinek_CutPointId">Sinek_CutPoint identifier</param>
        /// <returns>Sinek_CutPoint</returns>
        public virtual Sinek_CutPoint GetSinek_CutPointById(int sinek_CutPointId)
        {
            if (sinek_CutPointId == 0)
                return null;

            string key = string.Format(SINEK_CUTPOINT_BY_ID_KEY, sinek_CutPointId);
            return _cacheManager.Get(key, () =>
            {
                var sinek_CutPoint = _sinek_CutPointRepository.GetById(sinek_CutPointId);
                return sinek_CutPoint;
            });
        }

        /// <summary>
        /// Inserts a sinek_CutPoint
        /// </summary>
        /// <param name="sinek_CutPoint">Sinek_CutPoint</param>
        public virtual void InsertSinek_CutPoint(Sinek_CutPoint sinek_CutPoint)
        {
            if (sinek_CutPoint == null)
                throw new ArgumentNullException("sinek_CutPoint");

            _sinek_CutPointRepository.Insert(sinek_CutPoint);

            //cache
            _cacheManager.RemoveByPrefix(SINEK_CUTPOINT_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(sinek_CutPoint);


            #region Here we handle ProductVariantAttributeValue
            ProductAttributeValue objProductAttributeValue = new ProductAttributeValue();
            objProductAttributeValue.ProductAttributeMappingId = ProductVariantAttributeIdForSinek_Cut;
            objProductAttributeValue.Name = sinek_CutPoint.Size;
            objProductAttributeValue.ManufacturerId = 0;
            objProductAttributeValue.PriceAdjustment = 0;
            objProductAttributeValue.WeightAdjustment = 0;
            objProductAttributeValue.IsPreSelected = false;
            objProductAttributeValue.DisplayOrder = 0;
            objProductAttributeValue.Sinek_CutPointId = sinek_CutPoint.Id;
            objProductAttributeValue.Sinek_ModelId = 0;
            _productAttributeValueRepository.Insert(objProductAttributeValue);
            _eventPublisher.EntityInserted(objProductAttributeValue);

            //Core.Data.DataSettingsManager dataSettingsManager = new DataSettingsManager();
            //string conString = dataSettingsManager.LoadSettings().DataConnectionString;
            //SqlConnection scCon = new SqlConnection();
            //scCon.ConnectionString = conString;
            //scCon.Open();

            //string sqlCommand = "INSERT INTO ProductVariantAttributeValue " +
            //                    "(ProductVariantAttributeId,Name,Sinek_CutPointId,PriceAdjustment,WeightAdjustment,IsPreSelected,DisplayOrder,Sinek_ModelId,ManufacturerId) " +
            //                    "VALUES(" + ProductVariantAttributeIdForSinek_Cut.ToString() + ",'" + sinek_CutPoint.Size.ToString() + "', " + sinek_CutPoint.Id + ",0,0,0,0,0,0) ";
            ////"INSERT INTO ProductVariantAttributeValue " +
            ////"(ProductVariantAttributeId,Name,Sinek_CutPointId,PriceAdjustment,WeightAdjustment,IsPreSelected,DisplayOrder) " +
            ////"VALUES(22,'" + sinek_CutPoint.CutPoint.ToString() + "', " + sinek_CutPoint.Id.ToString() + ",0,0,0,0) ";

            //SqlCommand scCom = new SqlCommand(sqlCommand, scCon);
            //scCom.ExecuteNonQuery();
            //scCon.Close();

            #endregion
        }

        /// <summary>
        /// Updates the sinek_CutPoint
        /// </summary>
        /// <param name="sinek_CutPoint">Sinek_CutPoint</param>
        public virtual void UpdateSinek_CutPoint(Sinek_CutPoint sinek_CutPoint)
        {
            if (sinek_CutPoint == null)
                throw new ArgumentNullException("sinek_CutPoint");

            _sinek_CutPointRepository.Update(sinek_CutPoint);

            //cache
            _cacheManager.RemoveByPrefix(SINEK_CUTPOINT_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(sinek_CutPoint);

            #region Here we handle ProductVariantAttributeValue
            //delete ProductAttributeValue 
            ProductAttributeValue objProductAttributeValue = _productAttributeValueRepository.Table.Where(x => x.ProductAttributeMappingId == ProductVariantAttributeIdForSinek_Cut && x.Sinek_CutPointId == sinek_CutPoint.Id).FirstOrDefault();
            if (objProductAttributeValue != null)
            {
                _productAttributeValueRepository.Delete(objProductAttributeValue);
                _eventPublisher.EntityDeleted(objProductAttributeValue);
            }

            //insert new 
            ProductAttributeValue objProductAttribute = new ProductAttributeValue();
            objProductAttribute.ProductAttributeMappingId = ProductVariantAttributeIdForSinek_Cut;
            objProductAttribute.Name = sinek_CutPoint.Size;
            objProductAttribute.ManufacturerId = 0;
            objProductAttribute.PriceAdjustment = 0;
            objProductAttribute.WeightAdjustment = 0;
            objProductAttribute.IsPreSelected = false;
            objProductAttribute.DisplayOrder = 0;
            objProductAttribute.Sinek_CutPointId = sinek_CutPoint.Id;
            objProductAttribute.Sinek_ModelId = 0;
            _productAttributeValueRepository.Insert(objProductAttribute);
            _eventPublisher.EntityInserted(objProductAttribute);

            //insert second attribute
            ProductAttributeValue objProductAttributeNew = new ProductAttributeValue();
            objProductAttributeNew.ProductAttributeMappingId = 22;
            objProductAttributeNew.Name = sinek_CutPoint.CutPoint;
            objProductAttributeNew.ManufacturerId = 0;
            objProductAttributeNew.PriceAdjustment = 0;
            objProductAttributeNew.WeightAdjustment = 0;
            objProductAttributeNew.IsPreSelected = false;
            objProductAttributeNew.DisplayOrder = 0;
            objProductAttributeNew.Sinek_CutPointId = sinek_CutPoint.Id;
            objProductAttributeNew.Sinek_ModelId = 0;
            _productAttributeValueRepository.Insert(objProductAttributeNew);
            _eventPublisher.EntityInserted(objProductAttributeNew);

            //Core.Data.DataSettingsManager dataSettingsManager = new DataSettingsManager();
            //string conString = dataSettingsManager.LoadSettings().DataConnectionString;
            //SqlConnection scCon = new SqlConnection();
            //scCon.ConnectionString = conString;
            //scCon.Open();

            //string sqlCommand = "DELETE FROM ProductVariantAttributeValue " +
            //    //"WHERE ProductVariantAttributeId in (21,22) AND Sinek_CutPointId = " + sinek_CutPoint.Id.ToString() + " " +
            //                    "WHERE ProductVariantAttributeId in (" + ProductVariantAttributeIdForSinek_Cut.ToString() + ") AND Sinek_CutPointId = " + sinek_CutPoint.Id.ToString() + " " +
            //                    "INSERT INTO ProductVariantAttributeValue " +
            //                   "(ProductVariantAttributeId,Name,Sinek_CutPointId,PriceAdjustment,WeightAdjustment,IsPreSelected,DisplayOrder,Sinek_ModelId,ManufacturerId) " +
            //                   "VALUES(" + ProductVariantAttributeIdForSinek_Cut.ToString() + ",'" + sinek_CutPoint.Size.ToString() + "', " + sinek_CutPoint.Id + ",0,0,0,0,0,0) " +
            //                   "INSERT INTO ProductVariantAttributeValue " +
            //                   "(ProductVariantAttributeId,Name,Sinek_CutPointId,PriceAdjustment,WeightAdjustment,IsPreSelected,DisplayOrder,Sinek_ModelId,ManufacturerId) " +
            //                   "VALUES(22,'" + sinek_CutPoint.CutPoint.ToString() + "', " + sinek_CutPoint.Id.ToString() + ",0,0,0,0,0,0) ";

            //SqlCommand scCom = new SqlCommand(sqlCommand, scCon);
            //scCom.ExecuteNonQuery();
            //scCon.Close();

            #endregion
        }

        public virtual IList<Sinek_CutPoint> GetSinek_CutPointByManufacturerIdModelId(int modelId, int manufacturerId, bool showHidden = false)
        {
            string key = string.Format(SINEK_CUTPOINT_ALL_KEY, modelId, manufacturerId);
            return _cacheManager.Get(key, () =>
            {
                var query = from sp in _sinek_CutPointRepository.Table
                            orderby sp.DisplayOrder
                            where sp.ManufacturerId == manufacturerId && sp.ModelId == modelId &&
                            (showHidden || sp.Published)
                             &&
                            !sp.Deleted
                            select sp;
                var sinek_CutPoint = query.ToList();
                return sinek_CutPoint;
            });
        }

        public virtual IList<Sinek_CutPoint> GetSinek_CutPointByModelIId(int modelId, int manufacturerId, bool showHidden = false)
        {
            string key = string.Format(SINEK_CUTPOINT_ALL_KEY, modelId, manufacturerId);
            return _cacheManager.Get(key, () =>
            {
                var query = from sp in _sinek_CutPointRepository.Table
                            orderby sp.DisplayOrder
                            where sp.ManufacturerId == manufacturerId && sp.ModelId == modelId
                            where (showHidden || sp.Published) &&
                            !sp.Deleted
                            select sp;
                var sinek_CutPoint = query.ToList();
                return sinek_CutPoint;
            });
        }

        public virtual IList<Sinek_CutPoint> GetSinek_CutPointByManufacturerIdModelIdSize(int modelId, int manufacturerId, string size, bool showHidden = false)
        {
            //string key = string.Format(SINEK_CUTPOINT_ALL_KEY, modelId, manufacturerId, size);
            //return _cacheManager.Get(key, () =>
            //{
            //    var query = from sp in _sinek_CutPointRepository.Table
            //                orderby sp.DisplayOrder
            //                where sp.ManufacturerId == manufacturerId && sp.ModelId == modelId && sp.Size == size && 
            //                (showHidden || sp.Published) &&
            //                !sp.Deleted
            //                select sp;
            //    var sinek_CutPoint = query.ToList();
            //    return sinek_CutPoint;
            //});
            var query = from sp in _sinek_CutPointRepository.Table
                        orderby sp.DisplayOrder
                        where sp.ManufacturerId == manufacturerId && sp.ModelId == modelId && sp.Size == size &&
                        (showHidden || sp.Published) &&
                        !sp.Deleted
                        select sp;
            var sinek_CutPoint = query.ToList();
            if (sinek_CutPoint.Count == 0)
            {
                var allCutPoints = GetAllSinek_CutPoints();
                sinek_CutPoint = allCutPoints.Where(i => i.ManufacturerId == manufacturerId && i.ModelId == modelId && i.Size.TrimEnd().TrimStart().ToLower() == size.TrimEnd().TrimStart().ToLower()).ToList();
            }
            if (sinek_CutPoint.Count == 0)
            {
                var cutpoints = GetSinek_CutPointByManufacturerIdModelId(modelId, manufacturerId);
                sinek_CutPoint = cutpoints.Where(i => i.Size.TrimEnd().TrimStart().ToLower() == size.TrimEnd().TrimStart().ToLower()).ToList();
            }
            return sinek_CutPoint;
        }

        #endregion
    }
}
