﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.NB;
using Nop.Core.Events;
using Nop.Services.Events;

namespace Nop.Services.NB
{
    /// <summary>
    /// Sinek_CutPoint service 
    /// </summary>
    public partial class Sinek_CutPoint_ViewService : ISinek_CutPoint_ViewService
    {
        #region Constants
        private const string SINEK_CUTPOINT_ALL_KEY = "Nop.vw_Sinek_CutPoint.all-{0}";
        private const string SINEK_CUTPOINT_BY_ID_KEY = "Nop.vw_Sinek_CutPoint.id-{0}";
        private const string SINEK_CUTPOINT_PATTERN_KEY = "Nop.vw_Sinek_CutPoint.";
        #endregion

        #region Fields
        private readonly IRepository<Sinek_CutPoint_View> _sinek_CutPoint_ViewRepository;
        private readonly IRepository<Sinek_CutPoint> _sinek_CutPointRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="sinek_CutPointRepository">Category repository</param>
        /// <param name="productSinek_CutPointRepository">ProductCategory repository</param>
        /// <param name="productRepository">Product repository</param>
        /// <param name="eventPublisher">Event published</param>
        public Sinek_CutPoint_ViewService(ICacheManager cacheManager,
            IRepository<Sinek_CutPoint> sinek_CutPointRepository,
            IRepository<Sinek_CutPoint_View> sinek_CutPoint_VIewRepository,
            IEventPublisher eventPublisher)
        {
            _cacheManager = cacheManager;
            _sinek_CutPointRepository = sinek_CutPointRepository;
            _eventPublisher = eventPublisher;
        }
        #endregion

        #region Methods

        /// <summary>
        /// Deletes a sinek_CutPoint
        /// </summary>
        /// <param name="sinek_CutPoint">Sinek_CutPoint</param>
        public virtual void DeleteSinek_CutPoint(Sinek_CutPoint sinek_CutPoint)
        {
            if (sinek_CutPoint == null)
                throw new ArgumentNullException("sinek_CutPoint");

            sinek_CutPoint.Deleted = true;
            UpdateSinek_CutPoint(sinek_CutPoint);
        }

        /// <summary>
        /// Gets all sinek_CutPoints
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_CutPoint collection</returns>
        public virtual IList<Sinek_CutPoint> GetAllSinek_CutPoints(bool showHidden = false)
        {
            string key = string.Format(SINEK_CUTPOINT_ALL_KEY, showHidden);
            return _cacheManager.Get(key, () =>
            {
                var query = from m in _sinek_CutPointRepository.Table
                            orderby m.ManufacturerId
                            where (showHidden || m.Published) &&
                            !m.Deleted
                            select m;
                var sinek_CutPoints = query.ToList();
                return sinek_CutPoints;
            });
        }

        /// <summary>
        /// Gets all sinek_CutPoints
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_CutPoints</returns>
        public virtual IPagedList<Sinek_CutPoint> GetAllSinek_CutPoints(int pageIndex, int pageSize, bool showHidden = false)
        {
            var sinek_CutPoints = GetAllSinek_CutPoints(showHidden);
            return new PagedList<Sinek_CutPoint>(sinek_CutPoints, pageIndex, pageSize);
        }

        /// <summary>
        /// Gets all sinek_CutPoints
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_CutPoint collection</returns>
        public virtual IList<Sinek_CutPoint_View> GetAllSinek_CutPoints_View(bool showHidden = false)
        {
            string key = string.Format(SINEK_CUTPOINT_ALL_KEY, showHidden);
            return _cacheManager.Get(key, () =>
            {
                var query = from m in _sinek_CutPoint_ViewRepository.Table
                            orderby m.ManufacturerId
                            where (showHidden || m.Published) &&
                            !m.Deleted
                            select m;
                var sinek_CutPoint_Views = query.ToList();
                return sinek_CutPoint_Views;
            });
        }

        /// <summary>
        /// Gets all sinek_CutPoints
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_CutPoints</returns>
        public virtual IPagedList<Sinek_CutPoint_View> GetAllSinek_CutPoints_View(int pageIndex, int pageSize, bool showHidden = false)
        {
            var sinek_CutPoint_Views = GetAllSinek_CutPoints_View(showHidden);
            return new PagedList<Sinek_CutPoint_View>(sinek_CutPoint_Views, pageIndex, pageSize);
        }

        /// <summary>
        /// Gets a sinek_CutPoint
        /// </summary>
        /// <param name="sinek_CutPointId">Sinek_CutPoint identifier</param>
        /// <returns>Sinek_CutPoint</returns>
        public virtual Sinek_CutPoint GetSinek_CutPointById(int sinek_CutPointId)
        {
            if (sinek_CutPointId == 0)
                return null;

            string key = string.Format(SINEK_CUTPOINT_BY_ID_KEY, sinek_CutPointId);
            return _cacheManager.Get(key, () =>
            {
                var sinek_CutPoint = _sinek_CutPointRepository.GetById(sinek_CutPointId);
                return sinek_CutPoint;
            });
        }

        /// <summary>
        /// Inserts a sinek_CutPoint
        /// </summary>
        /// <param name="sinek_CutPoint">Sinek_CutPoint</param>
        public virtual void InsertSinek_CutPoint(Sinek_CutPoint sinek_CutPoint)
        {
            if (sinek_CutPoint == null)
                throw new ArgumentNullException("sinek_CutPoint");

            _sinek_CutPointRepository.Insert(sinek_CutPoint);

            //cache
            _cacheManager.RemoveByPrefix(SINEK_CUTPOINT_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(sinek_CutPoint);
        }

        /// <summary>
        /// Updates the sinek_CutPoint
        /// </summary>
        /// <param name="sinek_CutPoint">Sinek_CutPoint</param>
        public virtual void UpdateSinek_CutPoint(Sinek_CutPoint sinek_CutPoint)
        {
            if (sinek_CutPoint == null)
                throw new ArgumentNullException("sinek_CutPoint");

            _sinek_CutPointRepository.Update(sinek_CutPoint);

            //cache
            _cacheManager.RemoveByPrefix(SINEK_CUTPOINT_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(sinek_CutPoint);
        }

        #endregion
    }
}
