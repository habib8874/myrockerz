﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.NB;

namespace Nop.Services.NB
{
	/// <summary>
	/// Sinek_Location service
	/// </summary>
	public partial interface ISinek_LocationService
    {
        ///// <summary>
        ///// Deletes a sinek_Location
        ///// </summary>
        ///// <param name="sinek_Location">Sinek_Location</param>
        void DeleteSinek_Location(Sinek_Location sinek_Location);

        /// <summary>
        /// Gets all sinek_Locations
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_Location collection</returns>
        IList<Sinek_Location> GetAllSinek_Locations(bool showHidden = false);

        /// <summary>
        /// Gets all sinek_Locations
        /// </summary>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Sinek_Locations</returns>
        IPagedList<Sinek_Location> GetAllSinek_Locations(string name1, string name2, int pageIndex, int pageSize, bool showHidden = false);

        ///// <summary>
        ///// Gets a sinek_Location
        ///// </summary>
        ///// <param name="sinek_LocationId">Sinek_Location identifier</param>
        ///// <returns>Sinek_Location</returns>
        Sinek_Location GetSinek_LocationById(int sinek_LocationId);

        ///// <summary>
        ///// Inserts a sinek_Location
        ///// </summary>
        ///// <param name="sinek_Location">Sinek_Location</param>
        void InsertSinek_Location(Sinek_Location sinek_Location);

        ///// <summary>
        ///// Updates the sinek_Location
        ///// </summary>
        ///// <param name="sinek_Location">Sinek_Location</param>
        void UpdateSinek_Location(Sinek_Location sinek_Location);

        ///// <summary>
        ///// Deletes sinek_Locations 
        ///// </summary>
        ///// <param name="location">location</param>
        void DeleteLocation(Sinek_Location location);

        ///// <summary>
        ///// Gets  sinek_Locations by Ids
        ///// </summary>
        ///// <param name="locationIds">Sinek_Location identifier</param>
        ///// <returns>Locations</returns>
        IList<Sinek_Location> GetLocationsByIds(int[] locationIds);
    }
}
