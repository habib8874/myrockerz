﻿using Nop.Core.Domain.Localization;
using System;

namespace Nop.Core.Domain.NB
{
	/// <summary>
	/// Represents a Sinek_Location
	/// </summary>
	public partial class Sinek_Location : BaseEntity, ILocalizedEntity
	{
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string LocationType { get; set; }
        public string Name1 { get; set; }
        public string Name2 { get; set; }
        public string LocationAddress { get; set; }
        public string PhoneNo1 { get; set; }
        public string PhoneNo2 { get; set; }
        public string Fax { get; set; }
        public string WebSite { get; set; }
        public string EMail { get; set; }
        public string Notes { get; set; }
        public bool SellsSk8tape { get; set; }
        public bool SellsRockerz { get; set; }
        public int PageSize { get; set; }
        public string PageSizeOptions { get; set; }
        public bool Published { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }
    }
}
